import 'package:talk_to_me/language.dart';
import 'package:talk_to_me/ui/home_screen.dart';
import 'package:test/test.dart';

void main() {
  group('HomeScreen', () {
    test('initial state', () {
      final homeScreen = HomeScreen(
        initialLanguage: Languages.english,
        appTitle: 'my_title',
      );

      expect(
        homeScreen.initialLanguage,
        Languages.english,
      );
      expect(
        homeScreen.appTitle,
        'my_title',
      );
    });
  });
}
