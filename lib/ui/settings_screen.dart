import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talk_to_me/cubit/settings_cubit.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/language.dart';

const String rabbitChar = '🐇';
const String turtleChar = '🐢';
const String flatMusicChar = '♭';
const String sharpMusicChar = '♯';

class SettingsScreen extends StatelessWidget {
  final Language initialLanguage;

  const SettingsScreen({
    required this.initialLanguage,
  });

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (BuildContext context) => SettingsCubit(
          RepositoryProvider.of<TextToSpeechInterface>(context),
          RepositoryProvider.of<HiveStorageInterface>(context),
          DataModel(language: initialLanguage),
        ),
        child: SettingsView(),
      );
}

class SettingsView extends StatefulWidget {
  const SettingsView();

  @override
  State<StatefulWidget> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  SettingsCubit get cubit => context.read<SettingsCubit>();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('settings'),
        ),
        body: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            children: [
              BlocBuilder<SettingsCubit, SettingsState>(
                builder: (context, state) {
                  return LanguageSelectorWidget(
                    selectedLanguage: state.language,
                  );
                },
              ),
              SizedBox(height: 16),
              Row(
                children: [
                  TextIcon(turtleChar),
                  Expanded(
                    child: _SpeechRateSlider(
                      initialValue: cubit.speechRate,
                    ),
                  ),
                  TextIcon(rabbitChar),
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  TextIcon(flatMusicChar),
                  Expanded(
                    child: _PitchSlider(
                      initialValue: cubit.pitch,
                    ),
                  ),
                  TextIcon(sharpMusicChar),
                ],
              ),
            ],
          ),
        ),
      );
}

class LanguageSelectorWidget extends StatelessWidget {
  final Language selectedLanguage;

  const LanguageSelectorWidget({required this.selectedLanguage});

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: Languages.allSupported.map((language) {
          final double opacity = language == selectedLanguage ? 1.0 : 0.4;
          return Opacity(
            opacity: opacity,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 2),
              child: OutlinedButton(
                child: Text(
                  language.flag,
                  style: Theme.of(context).textTheme.headline6,
                ),
                onPressed: () => context.read<SettingsCubit>().selectLanguage(language),
              ),
            ),
          );
        }).toList(),
      );
}

class TextIcon extends StatelessWidget {
  final String text;
  late final double opacity;
  late final double bottomPadding;

  TextIcon(this.text, {Key? key}) : super(key: key) {
    opacity = text == turtleChar ? 0.8 : 1.0;
    if (text == turtleChar) {
      bottomPadding = 6.0;
    } else if (text == rabbitChar) {
      bottomPadding = 10.0;
    } else {
      bottomPadding = 12.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    final baseTextStyle =
        text == flatMusicChar ? Theme.of(context).textTheme.headline4! : Theme.of(context).textTheme.headline5!;
    final baseIcon = Padding(
      padding: EdgeInsets.only(bottom: bottomPadding),
      child: SizedBox(
        width: 28,
        height: 40,
        child: Center(
          child: Opacity(
            opacity: opacity,
            child: Text(
              text,
              style: baseTextStyle.copyWith(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );

    if ([rabbitChar, turtleChar].contains(text)) {
      return Transform(
        alignment: Alignment.center,
        transform: Matrix4.rotationY(pi),
        child: baseIcon,
      );
    } else {
      return baseIcon;
    }
  }
}

class _SpeechRateSlider extends StatefulWidget {
  final double initialValue;

  const _SpeechRateSlider({Key? key, required this.initialValue}) : super(key: key);

  @override
  State<_SpeechRateSlider> createState() => _SpeechRateSliderState();
}

class _SpeechRateSliderState extends State<_SpeechRateSlider> {
  late double value;

  SettingsCubit get cubit => context.read<SettingsCubit>();

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Slider(
      value: value,
      min: 0.05,
      max: 1,
      onChanged: (newValue) => setState(() => value = newValue),
      onChangeStart: (startingValue) async => await cubit.stopSpeaking(),
      onChangeEnd: (endingValue) async {
        await cubit.setSpeechRate(endingValue);
        await cubit.speakSampleText();
      },
    );
  }
}

class _PitchSlider extends StatefulWidget {
  final double initialValue;

  const _PitchSlider({Key? key, required this.initialValue}) : super(key: key);

  @override
  State<_PitchSlider> createState() => _PitchSliderState();
}

class _PitchSliderState extends State<_PitchSlider> {
  late double value;

  SettingsCubit get cubit => context.read<SettingsCubit>();

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Slider(
      value: value,
      min: 0.5,
      max: 2,
      onChanged: (newValue) => setState(() => value = newValue),
      onChangeStart: (startingValue) async => await cubit.stopSpeaking(),
      onChangeEnd: (endingValue) async {
        await cubit.setPitch(endingValue);
        await cubit.speakSampleText();
      },
    );
  }
}
