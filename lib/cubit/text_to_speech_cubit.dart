import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/clipboard_interface.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/interfaces/web_utils.dart';
import 'package:talk_to_me/language.dart';

part 'text_to_speech_state.dart';

class TextToSpeechCubit extends HydratedCubit<TextToSpeechState> {
  final ClipboardInterface _clipboardInterface;
  final TextToSpeechInterface _textToSpeechInterface;
  final HiveStorageInterface _hiveInterface;
  final WebUtils _webUtils;
  final DataModel _dataModel;

  // todo - consider passing methods, not the controller
  //   The controller is an object which belongs to the UI.
  //   Its methods could be passed as callbacks.
  AnimationController? playPauseAnimationController;

  TextToSpeechCubit(
    this._clipboardInterface,
    this._hiveInterface,
    this._textToSpeechInterface,
    this._webUtils,
    this._dataModel,
  ) : super(TextToSpeechStateInitial(text: _dataModel.language.explanationText)) {
    _textToSpeechInterface.setLanguage(_dataModel.language);
    _textToSpeechInterface.setCompletionCallback(() {
      if (_dataModel.isIndexOfActiveSectionAtMax) {
        _dataModel.toggleIsPlaying();
        _animatePlayPauseButton();
      } else {
        _dataModel.incrementIndexOfActiveSection();
        scrollNextToActiveSection();
        emitStateLoaded();
        _textToSpeechInterface.speak(_dataModel.activeSection);
      }
    });
  }

  void emitStateLoaded() {
    emit(TextToSpeechStateLoaded(
      text: _dataModel.text,
      indexOfActiveSection: _dataModel.indexOfActiveSection,
      percentComplete: _dataModel.percentBeforeActiveSection,
    ));
  }

  void emitStateLoading() => emit(TextToSpeechStateLoading());

  Future<void> pasteTextFromClipboard() async {
    emitStateLoading();
    if (_dataModel.isPlaying) {
      _dataModel.toggleIsPlaying();
      _animatePlayPauseButton();
      await _textToSpeechInterface.stop();
    }
    final Iterable<String> textFromClipboard = await _clipboardInterface.getText();
    if (textFromClipboard.isUrl) {
      final response = await _webUtils.httpGet(textFromClipboard.first);
      _dataModel.setText(response.texts);
    } else {
      _dataModel.setText(textFromClipboard);
    }
    _dataModel.resetIndexOfActiveSection();
    emitStateLoaded();
  }

  void reset() {
    if (_dataModel.isPlaying) {
      _dataModel.toggleIsPlaying();
      _animatePlayPauseButton();
      _textToSpeechInterface.stop();
    }
    _dataModel.resetText();
    _dataModel.resetIndexOfActiveSection();
    emitStateLoaded();
  }

  Future<void> playPause() async {
    _dataModel.toggleIsPlaying();
    _animatePlayPauseButton();
    if (_dataModel.isPlaying) {
      await _textToSpeechInterface.speak(_dataModel.activeSection);
    } else {
      await _textToSpeechInterface.stop();
    }
  }

  Future<void> navigateToSettings() async {
    if (_dataModel.isPlaying) {
      _dataModel.toggleIsPlaying();
      _animatePlayPauseButton();
      await _textToSpeechInterface.stop();
    }
  }

  void navigateFromSettings(bool wasExplanationText) {
    final Language newLanguage = _hiveInterface.language;
    if (_dataModel.getLanguage() != newLanguage) {
      _dataModel.setLanguage(newLanguage);
    }
    if (wasExplanationText) {
      _dataModel.setText(newLanguage.explanationText);
      emitStateLoaded();
    }
  }

  void _animatePlayPauseButton() {
    if (playPauseAnimationController != null) {
      if (_dataModel.isPlaying) {
        playPauseAnimationController!.forward();
      } else {
        playPauseAnimationController!.reverse();
      }
    }
  }

  Future<void> goToPreviousSection() async {
    if (_dataModel.indexOfActiveSection > 0) {
      _dataModel.decrementIndexOfActiveSection();
      if (_dataModel.isPlaying) {
        await _textToSpeechInterface.speak(_dataModel.activeSection);
      }
      emitStateLoaded();
    }
  }

  Future<void> goToNextSection() async {
    if (!_dataModel.isIndexOfActiveSectionAtMax) {
      _dataModel.incrementIndexOfActiveSection();
      if (_dataModel.isPlaying) {
        await _textToSpeechInterface.speak(_dataModel.activeSection);
      }
      emitStateLoaded();
    }
  }

  Future<void> skipToFirstSection() async {
    if (_dataModel.isPlaying) {
      await _textToSpeechInterface.stop();
      _dataModel.toggleIsPlaying();
      _animatePlayPauseButton();
    }
    _dataModel.resetIndexOfActiveSection();
    emitStateLoaded();
  }

  Future<void> skipToLastSection() async {
    if (_dataModel.isPlaying) {
      await _textToSpeechInterface.stop();
      _dataModel.toggleIsPlaying();
      _animatePlayPauseButton();
    }
    _dataModel.setIndexOfActiveSectionToMax();
    emitStateLoaded();
  }

  Future<void> setPitchFromStorage() async {
    final double pitchFromStorage = _hiveInterface.pitch;
    await _textToSpeechInterface.setPitch(pitchFromStorage);
  }

  Future<void> setSpeechRateFromStorage() async {
    final double speechRateFromStorage = _hiveInterface.speechRate;
    await _textToSpeechInterface.setSpeechRate(speechRateFromStorage);
  }

  Language getLanguage() => _hiveInterface.language;

  int get indexOfActiveSection => _dataModel.indexOfActiveSection;

  int get maxPossibleIndex => _dataModel.maxPossibleIndex;

  Duration getScrollDurationForSectionAtIndex(int index) {
    final int charsInSection = _dataModel.text.elementAt(index).length;
    return Duration(milliseconds: 150 + (charsInSection / 2).round());
  }

  VoidCallback scrollNextToActiveSection = () {};

  void setScrollNextToActiveSectionCallback(VoidCallback callback) => scrollNextToActiveSection = callback;

  bool get isAppInForeground => _dataModel.isAppInForeground;

  void setIsAppInForeground(bool newValue) => _dataModel.setIsAppInForeground(newValue);

  int get percentInActiveSection => _dataModel.percentInActiveSection;

  bool get isExplanationText {
    final String dataModelTextAsString = _dataModel.text.toString();
    return Languages.allSupported.any(
      (language) => dataModelTextAsString == language.explanationText.toString(),
    );
  }

  @override
  TextToSpeechState? fromJson(Map<String, dynamic> json) {
    if (json.isNotEmpty) {
      if (json['text'] != null) {
        _dataModel.setText(json['text']);
      }
      if (json['indexOfActiveSection'] != null) {
        _dataModel.setIndexOfActiveSection(json['indexOfActiveSection']);
      }
      return TextToSpeechStateLoaded.fromMap(json);
    }
  }

  @override
  Map<String, dynamic>? toJson(TextToSpeechState state) {
    if (state.runtimeType == TextToSpeechStateLoaded) {
      return state.toMap();
    } else {
      return null;
    }
  }
}
