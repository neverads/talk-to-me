import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:talk_to_me/cubit/settings_cubit.dart';
import 'package:talk_to_me/language.dart';
import 'package:talk_to_me/ui/settings_screen.dart';

class MockSettingsCubit extends MockCubit<SettingsState> implements SettingsCubit {}

class SettingsStateFake extends Fake implements SettingsState {}

void main() {
  setUpAll(() => registerFallbackValue<SettingsState>(SettingsStateFake()));

  group('SettingsView', () {
    late SettingsCubit settingsCubit;

    setUp(() {
      settingsCubit = MockSettingsCubit();
      when(() => settingsCubit.state).thenReturn(SettingsState(language: Languages.english));
      when(() => settingsCubit.speechRate).thenReturn(0.5);
      when(() => settingsCubit.pitch).thenReturn(1);
    });

    group('load a fresh settings-screen (no actions)', () {
      testWidgets('renders correct UI and triggers methods', (tester) async {
        final appBarTitleText = 'settings';
        final settingsViewForTesting = MaterialApp(home: SettingsView());
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: settingsCubit,
            child: settingsViewForTesting,
          ),
        );
        await tester.pumpAndSettle();

        verify(() => settingsCubit.pitch).called(1);
        verify(() => settingsCubit.speechRate).called(1);

        expect(find.byType(AppBar), findsOneWidget);
        expect(find.text(appBarTitleText), findsOneWidget);

        expect(find.byType(LanguageSelectorWidget), findsOneWidget);

        final slidersFinder = find.byType(Slider);
        expect(slidersFinder, findsNWidgets(2));

        final slidersEvaluated = slidersFinder.evaluate();
        final firstSlider = slidersEvaluated.first.widget as Slider;
        final secondSlider = slidersEvaluated.last.widget as Slider;

        expect(firstSlider.value, 0.5);
        // A default `min` of `0.0` seems to be ignored by the interfaces, so a non-0 `min` is needed.
        expect(firstSlider.min, 0.05);
        expect(firstSlider.max, 1);

        expect(secondSlider.value, 1);
        expect(secondSlider.min, 0.5);
        expect(secondSlider.max, 2);
      });
    });

    group('load the language-selector widget', () {
      testWidgets('renders correct UI & calls cubit when tapping buttons', (tester) async {
        final languageSelectorWidgetForTesting = MaterialApp(
          home: LanguageSelectorWidget(
            selectedLanguage: Languages.spanish,
          ),
        );
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: settingsCubit,
            child: languageSelectorWidgetForTesting,
          ),
        );

        final buttonFinder = find.byType(OutlinedButton);
        expect(buttonFinder, findsNWidgets(4));

        expect(find.text('🇺🇸'), findsOneWidget);
        expect(find.text('🇪🇸'), findsOneWidget);
        expect(find.text('🇫🇷'), findsOneWidget);
        expect(find.text('🇩🇪'), findsOneWidget);

        final opacityFinder = find.byType(Opacity);
        expect(opacityFinder, findsNWidgets(4));
        final opacityEvaluated = opacityFinder.evaluate();
        expect((opacityEvaluated.elementAt(0).widget as Opacity).opacity, 0.4);
        expect((opacityEvaluated.elementAt(1).widget as Opacity).opacity, 1.0);
        expect((opacityEvaluated.elementAt(2).widget as Opacity).opacity, 0.4);
        expect((opacityEvaluated.elementAt(3).widget as Opacity).opacity, 0.4);

        await tester.tap(buttonFinder.last);
        verify(() => settingsCubit.selectLanguage(Languages.german)).called(1);
      });
    });

    group('adjust speech-rate-slider', () {
      testWidgets(
        'triggers stopTalking, setSpeechRate',
        (tester) async {
          when(settingsCubit.stopSpeaking).thenAnswer((_) async => null);
          when(settingsCubit.speakSampleText).thenAnswer((_) async => null);
          when(() => settingsCubit.setSpeechRate(any())).thenAnswer((_) async => null);

          final settingsViewForTesting = MaterialApp(home: SettingsView());
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: settingsCubit,
              child: settingsViewForTesting,
            ),
          );

          await tester.drag(find.byType(Slider).first, Offset(100, 0));
          verify(settingsCubit.stopSpeaking).called(1);
          await tester.pump();
          final expectedNewSliderValue = 0.671604938271605;
          expect((find.byType(Slider).evaluate().first.widget as Slider).value, expectedNewSliderValue);
          verify(() => settingsCubit.setSpeechRate(expectedNewSliderValue)).called(1);
          verify(settingsCubit.speakSampleText).called(1);
        },
      );
    });

    group('adjust pitch-slider', () {
      testWidgets(
        'triggers stopTalking, setPitch',
        (tester) async {
          when(settingsCubit.stopSpeaking).thenAnswer((_) async => null);
          when(settingsCubit.speakSampleText).thenAnswer((_) async => null);
          when(() => settingsCubit.setPitch(any())).thenAnswer((_) async => null);

          final settingsViewForTesting = MaterialApp(home: SettingsView());
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: settingsCubit,
              child: settingsViewForTesting,
            ),
          );

          await tester.drag(find.byType(Slider).last, Offset(100, 0));
          await tester.pump();
          final expectedNewSliderValue = 1.4814814814814814;
          expect((find.byType(Slider).evaluate().last.widget as Slider).value, expectedNewSliderValue);
          verify(settingsCubit.stopSpeaking).called(1);
          verify(() => settingsCubit.setPitch(expectedNewSliderValue)).called(1);
          verify(settingsCubit.speakSampleText).called(1);
        },
      );
    });
  });
}
