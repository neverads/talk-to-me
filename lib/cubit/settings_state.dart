part of 'settings_cubit.dart';

@immutable
class SettingsState extends Equatable {
  final Language language;

  const SettingsState({required this.language});

  @override
  String toString() => '$runtimeType(language: $language)';

  @override
  List<Object?> get props => [this.language];
}
