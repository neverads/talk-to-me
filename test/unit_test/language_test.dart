import 'package:talk_to_me/language.dart';
import 'package:test/test.dart';

void main() {
  group('Language class', () {
    late Language language;

    setUp(() {
      language = Language(
        locale: 'locale-string',
        flag: 'a_flag',
        explanationText: ['placeholder', 'explanation'],
        sampleText: 'PC load letter',
        jumpToStartText: 'PC',
        jumpToEndText: 'load letter',
      );
    });

    test('initial state', () {
      expect(language.locale, 'locale-string');
      expect(language.flag, 'a_flag');
      expect(language.explanationText, ['placeholder', 'explanation']);
    });

    test('toString', () {
      expect(
        Language(
          locale: '',
          flag: '',
          explanationText: [],
          sampleText: '',
          jumpToStartText: '',
          jumpToEndText: '',
        ).toString(),
        'Language { locale: "", flag: "" }',
      );
      expect(
        language.toString(),
        'Language { locale: "locale-string", flag: "a_flag" }',
      );
    });

    test('`==` (equality)', () {
      final otherLanguage = Language(
        locale: 'locale-string',
        flag: 'a_flag',
        explanationText: ['placeholder', 'explanation'],
        sampleText: 'PC load letter',
        jumpToStartText: 'PC',
        jumpToEndText: 'load letter',
      );
      expect(language, otherLanguage);
    });

    test('factory-init', () {
      expect(Language.fromLocale(Languages.english.locale), Languages.english);
      expect(Language.fromLocale(Languages.spanish.locale), Languages.spanish);
      expect(Language.fromLocale(Languages.french.locale), Languages.french);
      expect(Language.fromLocale(Languages.german.locale), Languages.german);
      expect(Language.fromLocale(''), Languages.english);
    });
  });

  group('Languages class', () {
    test('languages exist and have correct content', () {
      expect(
        Languages.english,
        Language(
          locale: 'en-US',
          flag: '🇺🇸',
          explanationText: [
            "Paste text by tapping the clipboard-button above.",
            "If you paste a URL (a link to a website), the text from that web-page will be displayed.",
            "Never Ads is supported entirely by donations; all our apps are free & open-source with no ads or data-collection.",
          ],
          sampleText: 'How does this sound?  Use the sliders to adjust the rate and pitch of the voice.',
          jumpToStartText: "Long-press to jump to the start.",
          jumpToEndText: "Long-press to jump to the end.",
        ),
      );
      expect(
        Languages.spanish,
        Language(
          locale: 'es',
          flag: '🇪🇸',
          explanationText: [
            "Pega el texto pulsando el botón de la tabilla con sujetapapeles.",
            "Si pegas aquí la página web, el texto de la página estará cargado.",
            "Nuestro código es de código abierto en gitlab.com/neverads.",
          ],
          sampleText: '¿Cómo suena esto?  Utiliza los controles deslizantes para ajustar el ritmo y el tono de la voz.',
          jumpToStartText: "Pulsa prolongadamente para saltar al inicio.",
          jumpToEndText: "Pulsa prolongadamente para saltar al final.",
        ),
      );
      expect(
        Languages.french,
        Language(
          locale: 'fr',
          flag: '🇫🇷',
          explanationText: [
            "Collez du texte en pressant l'icone porte-bloc.",
            "Le texte d'une page internet s'affichera en collant son adresse ici.",
            "Notre code est open-source sur gitlab.com/neverads.",
          ],
          sampleText:
              'Comment cela sonne-t-il ?  Utilisez les curseurs pour régler le rythme et la hauteur de la voix.',
          jumpToStartText: "Appuyez longuement pour sauter au début.",
          jumpToEndText: "Appuyez longuement pour aller à la fin.",
        ),
      );
      expect(
        Languages.german,
        Language(
          locale: 'de',
          flag: '🇩🇪',
          explanationText: [
            "Füge Text ein indem du auf den Zwischenablagebutton drügst.",
            "Wenn du eine Web-Adresse einfügst, wird der Text der Website angezeigt.",
            "Unser Code ist Open-Source unter gitlab.com/neverads.",
          ],
          sampleText:
              'Wie klingt das?  Verwenden Sie die Schieberegler, um die Geschwindigkeit und die Tonhöhe der Stimme einzustellen.',
          jumpToStartText: "Drücken Sie lange, um zum Start zu springen.",
          jumpToEndText: "Drücken Sie lange, um zum Ende zu springen.",
        ),
      );
    });

    test('supported languages', () {
      expect(
        Languages.allSupported,
        [Languages.english, Languages.spanish, Languages.french, Languages.german],
      );
    });
  });
}
