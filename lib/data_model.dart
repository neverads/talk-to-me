import 'package:collection/collection.dart';
import 'package:talk_to_me/language.dart';

class DataModel {
  DataModel({required this.language});

  Language language;
  bool isAppInForeground = true;
  bool isPlaying = false;
  Iterable<String>? _text;
  int _indexOfActiveSection = 0;

  Language getLanguage() => language;

  void setLanguage(Language newLanguage) => language = newLanguage;

  void setIsAppInForeground(bool newValue) => isAppInForeground = newValue;

  void toggleIsPlaying() => isPlaying = !isPlaying;

  Iterable<String> get text => _text ?? _backupText;

  Iterable<String> get _backupText => language.explanationText;

  void setText(Iterable<String> newText) => _text = newText;

  void resetText() => _text = null;

  int get indexOfActiveSection => _indexOfActiveSection;

  void decrementIndexOfActiveSection() {
    if (indexOfActiveSection > 0) {
      _indexOfActiveSection--;
    } else {
      throw CannotDecrementError();
    }
  }

  void incrementIndexOfActiveSection() {
    if (indexOfActiveSection < maxPossibleIndex) {
      _indexOfActiveSection++;
    } else {
      throw CannotIncrementError(indexOfActiveSection, maxPossibleIndex);
    }
  }

  void resetIndexOfActiveSection() => _indexOfActiveSection = 0;

  void setIndexOfActiveSection(int newIndex) => _indexOfActiveSection = newIndex;

  void setIndexOfActiveSectionToMax() => _indexOfActiveSection = _maxPossibleIndex;

  String get activeSection => text.elementAt(indexOfActiveSection);

  int get maxPossibleIndex => _maxPossibleIndex;

  int get _maxPossibleIndex => text.length - 1;

  bool get isIndexOfActiveSectionAtMax => _indexOfActiveSection == _maxPossibleIndex;

  int get totalCharsInText => text.map((section) => section.length).sum;

  int get percentInActiveSection => (activeSection.length / totalCharsInText * 100).round();

  int get percentBeforeActiveSection {
    if (indexOfActiveSection == 0) {
      return 0;
    } else {
      int charactersInPreviousSections = 0;
      for (int i = 0; i < indexOfActiveSection; i++) {
        charactersInPreviousSections += text.elementAt(i).length;
      }
      final actualPercent = (charactersInPreviousSections / totalCharsInText * 100).round();
      if (actualPercent == 0) {
        return 1; // todo -- test this min-value
      } else if (actualPercent == 100) {
        return 99; // todo -- test this max-value
      } else {
        return actualPercent;
      }
    }
  }
}

class CannotDecrementError extends UnsupportedError {
  CannotDecrementError() : super('Unable to decrement index below 0.');

  @override
  bool operator ==(Object other) => other is CannotDecrementError && other.runtimeType == runtimeType;

  @override
  int get hashCode => 'Unable to decrement index below 0.'.hashCode;
}

class CannotIncrementError extends UnsupportedError {
  final int index;
  final int maxPossibleIndex;

  CannotIncrementError(this.index, this.maxPossibleIndex)
      : super('Unable to increment.  Index: $index.  Max possible: $maxPossibleIndex.');

  @override
  bool operator ==(Object other) =>
      other is CannotIncrementError &&
      other.runtimeType == runtimeType &&
      other.index == index &&
      other.maxPossibleIndex == maxPossibleIndex;

  @override
  int get hashCode => 'Index: $index.  Max possible: $maxPossibleIndex.'.hashCode;
}
