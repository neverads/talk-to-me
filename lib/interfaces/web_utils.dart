import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:validators/validators.dart';

class WebUtils {
  const WebUtils();

  Future<Response> httpGet(String urlAsString) async {
    Uri url = Uri.parse(urlAsString.trim());
    return await get(url);
  }
}

extension WebsiteParser on Response {
  String get urlOrPlaceholder {
    if (this.request == null) {
      return 'this website';
    } else
      return '"${this.request?.url.toString()}"';
  }

  Iterable<String> get texts {
    try {
      if (this.statusCode == 200) {
        final Iterable<String> texts = parse(this.body).texts;
        if (texts.isEmpty) {
          return [
            'Unable to find header or paragraph text on ${this.urlOrPlaceholder}.',
            '(For nerds: this app captures text from "header" & "paragraph" html-tags.)',
            'Please check the website by going to it directly to see if this is expected or not.',
            'If this was not expected, you can email us:',
            'never.ads.info@gmail.com',
          ];
        } else {
          return texts;
        }
      } else {
        return [
          'Unable to connect to ${this.urlOrPlaceholder}.',
          'The website is not accepting our attempt to connect to it.'
              'We expect them to tell us that everything is okay via a "200" code, '
              'but they sent a "${this.statusCode}" instead.',
          'A "404"/"400"/etc means that this website does not exist or cannot be accessed.',
          'Please try again, and if this persists, email us:',
          'never.ads.info@gmail.com',
        ];
      }
    } catch (e) {
      return [
        'Unable to connect to ${this.urlOrPlaceholder}.',
        '1. Please check your connection.',
        '2. Please check for typos in the URL.',
        'If you still have issues, you can email us:',
        'never.ads.info@gmail.com',
        'The specific error was: "$e"',
      ];
    }
  }
}

extension UrlChecker on Iterable<dynamic> {
  bool get isUrl {
    return this.length == 1 && this.first.runtimeType == String && isURL(this.first);
  }
}

extension TextExtractor on Document {
  Iterable<String> get texts {
    const _tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p'];
    final textElements = this.querySelectorAll('*').where((e) => _tags.contains(e.localName));
    final texts = textElements.map((e) => e.text.trim()).where((text) => text.isNotEmpty);
    return texts;
  }
}
