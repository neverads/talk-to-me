import 'package:hive_flutter/hive_flutter.dart';
import 'package:talk_to_me/language.dart';

HiveInterface hive = Hive; // allows for easy testing

class HiveStorageInterface {
  // null for prod; mock-hive for testing
  HiveStorageInterface({HiveInterface? hive}) : _hive = hive ?? Hive;

  final HiveInterface _hive;

  static const String generalStorageBoxName = 'box_name_general';

  static const String generalStorageBoxKeyLanguageFlag = '__generalStorageBoxKeyLanguageFlag';
  static const String generalStorageBoxKeyPitch = '__generalStorageBoxKeyPitch';
  static const String generalStorageBoxKeySpeechRate = "__generalStorageBoxKeySpeechRate";

  Future<void> openStorageBox() async {
    await _hive.openBox(generalStorageBoxName);
  }

  Box get storageBox => _hive.box(generalStorageBoxName);

  // todo -- Untested; test me!  (Mock `Box`?)
  Language get language {
    final String locale = storageBox.get(
      generalStorageBoxKeyLanguageFlag,
      defaultValue: Languages.english.locale,
    );
    return Language.fromLocale(locale);
  }

  // todo -- Untested; test me!  (Mock `Box`?)
  double get pitch => storageBox.get(
        generalStorageBoxKeyPitch,
        defaultValue: 1.0,
      );

  // todo -- Untested; test me!  (Mock `Box`?)
  double get speechRate => storageBox.get(
        generalStorageBoxKeySpeechRate,
        defaultValue: 0.5,
      );

  // todo -- Untested; test me!  (Mock `Box`?)
  void saveLanguageLocaleToStorage(String newLanguageLocale) {
    storageBox.put(generalStorageBoxKeyLanguageFlag, newLanguageLocale);
  }

  // todo -- Untested; test me!  (Mock `Box`?)
  void setPitch(double newPitch) {
    storageBox.put(generalStorageBoxKeyPitch, newPitch);
  }

  // todo -- Untested; test me!  (Mock `Box`?)
  void setSpeechRate(double newSpeechRate) {
    storageBox.put(generalStorageBoxKeySpeechRate, newSpeechRate);
  }
}
