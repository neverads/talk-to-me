import 'package:flutter_test/flutter_test.dart';
import 'package:talk_to_me/cubit/settings_cubit.dart';
import 'package:talk_to_me/cubit/text_to_speech_cubit.dart';
import 'package:talk_to_me/language.dart';

void main() {
  group('TextToSpeechState', () {
    group('loading', () {
      test('2 different ones are equal', () {
        expect(TextToSpeechStateLoading(), TextToSpeechStateLoading());
      });

      test('toString', () {
        expect(TextToSpeechStateLoading().toString(), 'TextToSpeechStateLoading');
      });

      test('props', () {
        expect(TextToSpeechStateLoading().props, ['TextToSpeechStateLoading']);
      });

      test('toMap', () {
        expect(TextToSpeechStateLoading().toMap(), {});
      });
    });

    group('Loaded', () {
      late TextToSpeechStateLoaded loaded;

      setUp(() {
        loaded = TextToSpeechStateLoaded(
          text: ['PC', 'load letter'],
          indexOfActiveSection: 1,
          percentComplete: 15,
        );
      });

      test('attributes', () {
        expect(loaded.text, ['PC', 'load letter']);
        expect(loaded.indexOfActiveSection, 1);
        expect(loaded.percentComplete, 15);
      });

      test('2 with same attributes are equal', () {
        expect(
          loaded,
          TextToSpeechStateLoaded(
            text: ['PC', 'load letter'],
            indexOfActiveSection: 1,
            percentComplete: 15,
          ),
        );
      });

      test('toString', () {
        expect(
          loaded.toString(),
          'TextToSpeechStateLoaded(text: ["PC", "load letter"], indexOfActiveSection: 1, percentComplete: 15)',
        );
        expect(
          TextToSpeechStateLoaded(text: [], indexOfActiveSection: 0, percentComplete: 0).toString(),
          'TextToSpeechStateLoaded(text: [], indexOfActiveSection: 0, percentComplete: 0)',
        );
      });

      test('props', () {
        expect(loaded.props, [loaded.text, loaded.indexOfActiveSection, loaded.percentComplete]);
      });

      test('toMap', () {
        expect(
          loaded.toMap(),
          {
            'text': ['PC', 'load letter'],
            'indexOfActiveSection': 1,
            'percentComplete': 15,
          },
        );
      });

      group('fromMap', () {
        test('good/expected map', () {
          expect(
            TextToSpeechStateLoaded.fromMap({
              'text': ['PC', 'load letter'],
              'indexOfActiveSection': 1,
              'percentComplete': 15,
            }),
            loaded,
          );
        });
        test('empty map', () {
          expect(
            TextToSpeechStateLoaded.fromMap({}),
            TextToSpeechStateLoaded(
              text: Languages.english.explanationText,
              indexOfActiveSection: 0,
              percentComplete: 0,
            ),
          );
        });

        test('bad map', () {
          expect(
            TextToSpeechStateLoaded.fromMap({'P': 'C', 'load': 'letter'}),
            TextToSpeechStateLoaded(
              text: Languages.english.explanationText,
              indexOfActiveSection: 0,
              percentComplete: 0,
            ),
          );
        });
      });

      group('toMap', () {
        test('converts state to map', () {
          expect(
              TextToSpeechStateLoaded(
                text: ['PC', 'load letter'],
                indexOfActiveSection: 0,
                percentComplete: 0,
              ).toMap(),
              {
                'text': ['PC', 'load letter'],
                'indexOfActiveSection': 0,
                'percentComplete': 0,
              });
        });
      });

      test('toJson', () {
        expect(
          loaded.toJson(),
          '{"text":["PC","load letter"],"indexOfActiveSection":1,"percentComplete":15}',
        );
      });

      test('fromJson', () {
        expect(
          TextToSpeechStateLoaded.fromJson(
            '{"text":["PC","load letter"],"indexOfActiveSection":1,"percentComplete":15}',
          ),
          loaded,
        );
      });
    });

    group('Initial', () {
      late TextToSpeechStateInitial initial;

      setUp(() {
        initial = TextToSpeechStateInitial(text: ['PC', 'load letter']);
      });

      test('attributes', () {
        expect(initial.text, ['PC', 'load letter']);
        expect(initial.indexOfActiveSection, 0);
        expect(initial.percentComplete, 0);
      });

      test('2 with same attributes are equal', () {
        expect(
          initial,
          TextToSpeechStateInitial(text: ['PC', 'load letter']),
        );
      });

      test('toString', () {
        expect(
          initial.toString(),
          'TextToSpeechStateInitial(text: ["PC", "load letter"], indexOfActiveSection: 0, percentComplete: 0)',
        );
        expect(
          TextToSpeechStateInitial(text: []).toString(),
          'TextToSpeechStateInitial(text: [], indexOfActiveSection: 0, percentComplete: 0)',
        );
      });

      test('props', () {
        expect(initial.props, [initial.text, 0, 0]);
      });

      test('toMap', () {
        expect(
          initial.toMap(),
          {
            'text': ['PC', 'load letter'],
            'indexOfActiveSection': 0,
            'percentComplete': 0,
          },
        );
      });

      test('toJson', () {
        expect(
          initial.toJson(),
          '{"text":["PC","load letter"],"indexOfActiveSection":0,"percentComplete":0}',
        );
      });
    });
  });

  group('SettingsState', () {
    late SettingsState state;

    setUp(() {
      state = SettingsState(language: Languages.english);
    });

    test('attributes', () {
      expect(state.language, Languages.english);
    });

    test('2 with same attributes are equal', () {
      expect(state, SettingsState(language: Languages.english));
    });

    test('toString', () {
      expect(
        state.toString(),
        'SettingsState(language: ${state.language})',
      );
    });

    test('props', () {
      expect(state.props, [state.language]);
    });
  });
}
