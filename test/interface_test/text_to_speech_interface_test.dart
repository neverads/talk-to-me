import 'package:flutter/foundation.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:mocktail/mocktail.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/language.dart';
import 'package:test/test.dart';

class MockFlutterTts extends Mock implements FlutterTts {}

void main() {
  group('TextToSpeechInterface', () {
    late FlutterTts flutterTts;
    late TextToSpeechInterface textToSpeechInterface;

    setUp(() {
      flutterTts = MockFlutterTts();
      textToSpeechInterface = TextToSpeechInterface(textToSpeech: flutterTts);
    });

    group('constructor', () {
      test('tries to instantiate FlutterTts when not injected', () {
        try {
          TextToSpeechInterface();
        } catch (error) {
          expect(error.toString(), 'Null check operator used on a null value');
        }
      });
    });

    group('speak', () {
      test('calls FlutterTts.speak with correct text', () async {
        const text = 'PC load letter';

        try {
          await textToSpeechInterface.speak(text);
        } catch (_) {}
        verify(() => flutterTts.speak(text)).called(1);
      });
    });

    group('stop', () {
      test('calls FlutterTts.stop', () async {
        try {
          await textToSpeechInterface.stop();
        } catch (_) {}
        verify(() => flutterTts.stop()).called(1);
      });
    });

    group('manipulating the completion-callback', () {
      group('setCompletionCallback', () {
        test('sets FlutterTts-completion-handler', () async {
          final VoidCallback callback = () => print('PC load letter');
          try {
            textToSpeechInterface.setCompletionCallback(callback);
          } catch (_) {}
          verify(() => flutterTts.setCompletionHandler(callback)).called(1);
        });
      });

      group('clearCompletionCallback', () {
        test('sets FlutterTts-completion-handler', () async {
          try {
            textToSpeechInterface.clearCompletionCallback();
          } catch (_) {}
          verify(() => flutterTts.setCompletionHandler(any())).called(1);
        });
      });

      group('resetCompletionCallback', () {
        test('resets FlutterTts-completion-handler', () async {
          try {
            textToSpeechInterface.restoreCompletionCallback();
          } catch (_) {}
          verify(() => flutterTts.setCompletionHandler(any())).called(1);
        });
      });
    });

    group('setSpeechRate', () {
      test('calls FlutterTts.setSpeechRate', () async {
        const rate = 0.42;

        try {
          await textToSpeechInterface.setSpeechRate(rate);
        } catch (_) {}
        verify(() => flutterTts.setSpeechRate(rate)).called(1);
      });
    });

    group('setPitch', () {
      test('calls FlutterTts.setPitch', () async {
        const pitch = 0.9;

        try {
          await textToSpeechInterface.setPitch(pitch);
        } catch (_) {}
        verify(() => flutterTts.setPitch(pitch)).called(1);
      });
    });

    group('setLanguage', () {
      test('calls FlutterTts.setLanguage with correct language', () async {
        [Languages.english, Languages.spanish, Languages.french, Languages.german].forEach((language) {
          try {
            textToSpeechInterface.setLanguage(language);
          } catch (_) {}
          verify(() => flutterTts.setLanguage(language.locale)).called(1);
        });
      });

      test('calls FlutterTts.setLanguage with default when no match', () async {
        try {
          textToSpeechInterface.setLanguage(Language(
            locale: '',
            flag: '',
            explanationText: [],
            sampleText: '',
            jumpToStartText: '',
            jumpToEndText: '',
          ));
        } catch (_) {}
        verify(() => flutterTts.setLanguage(Languages.english.locale)).called(1);
      });
    });
  });
}
