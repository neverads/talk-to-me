import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:talk_to_me/interfaces/clipboard_interface.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/interfaces/web_utils.dart';
import 'package:talk_to_me/ui/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final storageDirectory = await getApplicationDocumentsDirectory();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: storageDirectory,
  );
  await Hive.initFlutter(storageDirectory.path);
  final _hiveInterface = HiveStorageInterface();
  await _hiveInterface.openStorageBox();
  runApp(
    TalkToMeApp(
      clipboardInterface: const ClipboardInterface(),
      hiveInterface: _hiveInterface,
      textToSpeechInterface: TextToSpeechInterface(),
      webUtils: const WebUtils(),
    ),
  );
}

class TalkToMeApp extends StatelessWidget {
  final ClipboardInterface clipboardInterface;
  final HiveStorageInterface hiveInterface;
  final TextToSpeechInterface textToSpeechInterface;
  final WebUtils webUtils;

  const TalkToMeApp({
    required this.clipboardInterface,
    required this.hiveInterface,
    required this.textToSpeechInterface,
    required this.webUtils,
  });

  static const MaterialColor primaryColor = Colors.deepPurple;
  static const String appTitle = "Talk to me";

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(value: clipboardInterface),
        RepositoryProvider.value(value: hiveInterface),
        RepositoryProvider.value(value: textToSpeechInterface),
        RepositoryProvider.value(value: webUtils),
      ],
      child: MaterialApp(
        title: appTitle,
        theme: ThemeData(
          primarySwatch: primaryColor,
          iconTheme: IconThemeData(color: primaryColor),
        ),
        home: HomeScreen(
          initialLanguage: hiveInterface.language,
          appTitle: appTitle,
        ),
      ),
    );
  }
}
