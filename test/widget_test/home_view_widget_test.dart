import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:talk_to_me/cubit/text_to_speech_cubit.dart';
import 'package:talk_to_me/language.dart';
import 'package:talk_to_me/ui/home_screen.dart';

class MockTextToSpeechCubit extends MockCubit<TextToSpeechState> implements TextToSpeechCubit {}

class TextToSpeechStateFake extends Fake implements TextToSpeechState {}

void main() {
  setUpAll(() => registerFallbackValue<TextToSpeechState>(TextToSpeechStateFake()));

  group('HomeView', () {
    //   const problemText = [
    //     "Hello, Peter.",
    //     "What's happening?",
    //     "Uh… we have sort of a problem here.",
    //     "Yeah - you apparently didn't put one of the new cover-sheets on your TPS-reports.",
    //   ];
    late TextToSpeechCubit textToSpeechCubit;
    final playPauseButton = find.byWidgetPredicate(
      (playPauseButton) {
        return playPauseButton is AnimatedIcon &&
            playPauseButton.icon == AnimatedIcons.play_pause &&
            playPauseButton.progress.value == 0.0;
      },
    );

    setUp(() {
      textToSpeechCubit = MockTextToSpeechCubit();
      when(textToSpeechCubit.setPitchFromStorage).thenAnswer((_) async => await null);
      when(textToSpeechCubit.setSpeechRateFromStorage).thenAnswer((_) async => await null);
      when(() => textToSpeechCubit.isAppInForeground).thenReturn(true);
      when(() => textToSpeechCubit.indexOfActiveSection).thenReturn(0);
      when(() => textToSpeechCubit.percentInActiveSection).thenReturn(0);
      when(() => textToSpeechCubit.getScrollDurationForSectionAtIndex(any())).thenReturn(Duration(milliseconds: 1));
    });

    group('ProgressWidget', () {
      testWidgets('no progress', (tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: ProgressWidget(),
          ),
        );
        final progressBarFinder = find.byType(LinearProgressIndicator);
        expect(progressBarFinder, findsNWidgets(2));
        final progressBarsEvaluated = progressBarFinder.evaluate();
        expect((progressBarsEvaluated.first.widget as LinearProgressIndicator).value, 0.0);
        expect((progressBarsEvaluated.last.widget as LinearProgressIndicator).value, 0.0);
      });

      testWidgets('some progress', (tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: ProgressWidget(
              percentComplete: 9,
              percentInActiveSection: 42,
            ),
          ),
        );
        final progressBarFinder = find.byType(LinearProgressIndicator);
        expect(progressBarFinder, findsNWidgets(2));
        final progressBarsEvaluated = progressBarFinder.evaluate();
        expect((progressBarsEvaluated.first.widget as LinearProgressIndicator).value, 0.51);
        expect((progressBarsEvaluated.last.widget as LinearProgressIndicator).value, 0.09);
      });
    });

    testWidgets('DisplayTextViewLoaded', (tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: DisplayTextViewLoaded(
            textToDisplay: ['PC', 'load letter'],
            indexOfActiveSection: 1,
            itemScrollController: ItemScrollController(),
            itemPositionsListener: ItemPositionsListener.create(),
          ),
        ),
      );
      expect(find.text('PC'), findsOneWidget);
      expect(find.text('load letter'), findsOneWidget);
    });

    testWidgets('DisplayTextViewLoading', (tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: DisplayTextViewLoading(),
        ),
      );
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.text('Getting text…'), findsOneWidget);
    });

    group('no actions (initial state)', () {
      testWidgets(
        'renders correct UI & calls cubit-helpers for initial set-up',
        (tester) async {
          when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: ['PC', 'load letter']));
          final homeViewForTesting = MaterialApp(home: HomeView(appTitle: 'Talk to me'));
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: textToSpeechCubit,
              child: homeViewForTesting,
            ),
          );

          // app-bar & children
          expect(find.byType(AppBar), findsOneWidget);
          expect(find.text('Talk to me'), findsOneWidget);
          expect(find.byIcon(Icons.paste), findsOneWidget);
          expect(find.byWidgetPredicate((widget) => widget is PopupMenuButton), findsOneWidget); // PopupMenuButton
          expect(find.byIcon(Icons.more_vert), findsOneWidget); // same PopupMenuButton as ^^

          expect(find.byType(DisplayTextViewLoaded), findsOneWidget);

          expect(find.byType(ProgressWidget), findsOneWidget);

          expect(find.byIcon(Icons.fast_rewind), findsOneWidget);
          expect(find.byIcon(Icons.skip_previous), findsOneWidget);
          expect(playPauseButton, findsOneWidget);
          expect(find.byIcon(Icons.skip_next), findsOneWidget);
          expect(find.byIcon(Icons.fast_forward), findsOneWidget);

          verify(textToSpeechCubit.setPitchFromStorage).called(1);
          verify(textToSpeechCubit.setSpeechRateFromStorage).called(1);
          verify(() => textToSpeechCubit.setScrollNextToActiveSectionCallback(any())).called(1);
          verify(() => textToSpeechCubit.percentInActiveSection).called(1);
        },
      );
    });

    testWidgets(
      'renders loading-UI when state is loading',
      (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateLoading());
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: 'Talk to me'));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );
        expect(find.byType(DisplayTextViewLoading), findsOneWidget);
      },
    );

    testWidgets(
      'renders loaded-UI when state is loaded',
      (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(
          TextToSpeechStateLoaded(
            text: [],
            indexOfActiveSection: 0,
            percentComplete: 0,
          ),
        );
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );
        expect(find.byType(DisplayTextViewLoaded), findsOneWidget);
      },
    );

    testWidgets('tapping paste-button gets text from clipboard', (tester) async {
      when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: ['PC', 'load letter']));
      final homeViewForTesting = MaterialApp(home: HomeView(appTitle: 'talk to me'));
      await tester.pumpWidget(
        RepositoryProvider.value(
          value: textToSpeechCubit,
          child: homeViewForTesting,
        ),
      );
      when(textToSpeechCubit.pasteTextFromClipboard).thenAnswer((_) async => null);

      await tester.tap(find.byIcon(Icons.paste));
      verify(textToSpeechCubit.pasteTextFromClipboard).called(1);
    });

    group('tap reset-button', () {
      testWidgets('resets text/UI to initial state', (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(
          TextToSpeechStateLoaded(
            text: ['PC', 'load letter'],
            indexOfActiveSection: 1,
            percentComplete: 15,
          ),
        );
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: 'talk to me'));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );
        when(textToSpeechCubit.reset).thenAnswer((_) async => null);

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();
        await tester.tap(find.byIcon(Icons.delete_sweep));
        await tester.pumpAndSettle();
        verify(textToSpeechCubit.reset).called(1);
        expect(find.text('reset'), findsNothing);
        expect(find.byIcon(Icons.more_vert), findsOneWidget);
      });
    });

    group('tap settings-button', () {
      testWidgets('triggers cubit-helpers', (tester) async {
        // todo -- assert settings-screen is launched & `navigateFromSettings` was triggered
        when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
        when(() => textToSpeechCubit.isExplanationText).thenReturn(true);
        when(textToSpeechCubit.navigateToSettings).thenAnswer((_) async => null);
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );

        await tester.tap(find.byIcon(Icons.more_vert));
        await tester.pumpAndSettle();
        await tester.tap(find.byIcon(Icons.settings));
        verify(textToSpeechCubit.navigateToSettings).called(1);
        verify(() => textToSpeechCubit.isExplanationText).called(1);
      });
    });

    group('tap play/pause button', () {
      testWidgets('calls cubit.playPause', (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );

        when(textToSpeechCubit.playPause).thenAnswer((_) async => null);
        await tester.tap(playPauseButton);
        verify(textToSpeechCubit.playPause).called(1);
      });
    });

    group('tap next button', () {
      testWidgets('calls cubit.goToNextSection & scrolls (if needed)', (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );
        when(textToSpeechCubit.goToNextSection).thenAnswer((_) async => null);
        when(() => textToSpeechCubit.indexOfActiveSection).thenReturn(1);
        await tester.tap(find.byIcon(Icons.skip_next));
        verify(textToSpeechCubit.goToNextSection).called(1);
        verify(() => textToSpeechCubit.indexOfActiveSection).called(3); // x1 to jump-scroll at launch; x2 for scroll
        verify(() => textToSpeechCubit.getScrollDurationForSectionAtIndex(0)).called(1);
      });
    });

    group('tap previous button', () {
      testWidgets('calls cubit.goToPreviousSection & scrolls (if needed)', (tester) async {
        when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
        final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: textToSpeechCubit,
            child: homeViewForTesting,
          ),
        );

        when(textToSpeechCubit.goToPreviousSection).thenAnswer((_) async => null);
        when(() => textToSpeechCubit.indexOfActiveSection).thenReturn(0);
        await tester.tap(find.byIcon(Icons.skip_previous));
        verify(textToSpeechCubit.goToPreviousSection).called(1);
        verify(() => textToSpeechCubit.indexOfActiveSection).called(3); // x1 to jump-scroll at launch; x2 for scroll
        verify(() => textToSpeechCubit.getScrollDurationForSectionAtIndex(0)).called(1);
      });
    });

    group('skipToLastSection button', () {
      group('long-press', () {
        testWidgets('calls cubit.skipToLastSection', (tester) async {
          when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
          final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: textToSpeechCubit,
              child: homeViewForTesting,
            ),
          );
          when(textToSpeechCubit.skipToLastSection).thenAnswer((_) async => null);
          when(() => textToSpeechCubit.indexOfActiveSection).thenReturn(0);
          await tester.longPress(find.byIcon(Icons.fast_forward));
          await tester.pumpAndSettle();
          verify(textToSpeechCubit.skipToLastSection).called(1);
          verify(() => textToSpeechCubit.indexOfActiveSection).called(3); // x1 to jump-scroll at launch; x2 for scroll
          verifyNever(() => textToSpeechCubit.getScrollDurationForSectionAtIndex(any()));
        });
      });

      group('tap', () {
        testWidgets('displays snack-bar', (tester) async {
          when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
          when(textToSpeechCubit.getLanguage).thenReturn(Languages.english);
          final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: textToSpeechCubit,
              child: homeViewForTesting,
            ),
          );
          expect(find.byType(SnackBar), findsNothing);
          await tester.tap(find.byIcon(Icons.fast_forward));
          verify(textToSpeechCubit.getLanguage).called(1);
          await tester.pump();
          expect(find.byType(SnackBar), findsOneWidget);
          expect(find.text(Languages.english.jumpToEndText), findsOneWidget);
        });
      });
    });

    group('skipToFirstSection button', () {
      group('long-press', () {
        testWidgets('calls cubit.skipToFirstSection', (tester) async {
          when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
          final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: textToSpeechCubit,
              child: homeViewForTesting,
            ),
          );
          when(textToSpeechCubit.skipToFirstSection).thenAnswer((_) async => null);
          when(() => textToSpeechCubit.indexOfActiveSection).thenReturn(0);
          await tester.longPress(find.byIcon(Icons.fast_rewind));
          await tester.pumpAndSettle();
          verify(textToSpeechCubit.skipToFirstSection).called(1);
          verify(() => textToSpeechCubit.indexOfActiveSection).called(3); // x1 to jump-scroll at launch; x2 for scroll
          verifyNever(() => textToSpeechCubit.getScrollDurationForSectionAtIndex(any()));
        });
      });

      group('tap', () {
        testWidgets('displays snack-bar', (tester) async {
          when(() => textToSpeechCubit.state).thenReturn(TextToSpeechStateInitial(text: []));
          when(textToSpeechCubit.getLanguage).thenReturn(Languages.spanish);
          final homeViewForTesting = MaterialApp(home: HomeView(appTitle: ''));
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: textToSpeechCubit,
              child: homeViewForTesting,
            ),
          );
          expect(find.byType(SnackBar), findsNothing);
          await tester.tap(find.byIcon(Icons.fast_rewind));
          verify(textToSpeechCubit.getLanguage).called(1);
          await tester.pump();
          expect(find.byType(SnackBar), findsOneWidget);
          expect(find.text(Languages.spanish.jumpToStartText), findsOneWidget);
        });
      });
    });
  });
}
