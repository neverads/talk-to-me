import 'package:bloc_test/bloc_test.dart';
import 'package:http/http.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:mocktail/mocktail.dart';
import 'package:talk_to_me/cubit/text_to_speech_cubit.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/clipboard_interface.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/interfaces/web_utils.dart';
import 'package:talk_to_me/language.dart';
import 'package:test/test.dart';

class MockStorage extends Mock implements Storage {}

class MockClipboardInterface extends Mock implements ClipboardInterface {}

class MockTextToSpeechInterface extends Mock implements TextToSpeechInterface {}

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class MockWebUtils extends Mock implements WebUtils {}

const Language mockLanguage = Language(
  locale: '_locale',
  flag: '_flag',
  explanationText: ['_explanation', '_text'],
  sampleText: 'sample text',
  jumpToStartText: 'jts',
  jumpToEndText: 'jte',
);

class MockDataModel extends Mock implements DataModel {
  @override
  Language get language => mockLanguage;
}

class LanguageFake extends Fake implements Language {}

void main() {
  setUpAll(() => registerFallbackValue<Language>(LanguageFake()));

  group('TextToSpeechCubit', () {
    final Iterable<String> pcLoadLetter = ['PC', 'load letter'];

    late Storage storage;

    late ClipboardInterface clipboardInterface;
    late TextToSpeechInterface textToSpeechInterface;
    late HiveStorageInterface hiveStorageInterface;
    late WebUtils webUtils;
    late DataModel dataModel;
    late TextToSpeechCubit cubit;

    TextToSpeechStateLoaded simplestLoadedState = TextToSpeechStateLoaded(
      text: [],
      indexOfActiveSection: 0,
      percentComplete: 0,
    );

    setUp(() {
      storage = MockStorage();
      when<dynamic>(() => storage.read(any())).thenReturn(<String, dynamic>{});
      when(() => storage.write(any(), any<dynamic>())).thenAnswer((_) async {});
      when(() => storage.delete(any())).thenAnswer((_) async {});
      when(() => storage.clear()).thenAnswer((_) async {});
      HydratedBloc.storage = storage;
      clipboardInterface = MockClipboardInterface();
      textToSpeechInterface = MockTextToSpeechInterface();
      hiveStorageInterface = MockHiveStorageInterface();
      webUtils = MockWebUtils();
      dataModel = MockDataModel();
      cubit = TextToSpeechCubit(
        clipboardInterface,
        hiveStorageInterface,
        textToSpeechInterface,
        webUtils,
        dataModel,
      );
      when(textToSpeechInterface.stop).thenAnswer((_) async => null); // will happen during `tearDown()`/`close()`

      // these 3 are used every time loaded-state is emitted
      when(() => dataModel.text).thenReturn([]);
      when(() => dataModel.indexOfActiveSection).thenReturn(0);
      when(() => dataModel.percentBeforeActiveSection).thenReturn(0);
    });

    tearDown(() => cubit.close());

    group('fromJson (hydration method)', () {
      test('normal json return state-object', () {
        final Map<String, dynamic> _json = {
          'text': ['PC', 'load letter'],
          'indexOfActiveSection': 9,
          'percentComplete': 42,
        };
        expect(cubit.fromJson(_json), TextToSpeechStateLoaded.fromMap(_json));
        verify(() => dataModel.setIndexOfActiveSection(9)).called(1);
        verify(() => dataModel.setText(['PC', 'load letter'])).called(1);
      });

      test('non-empty json return state-object', () {
        final Map<String, dynamic> _json = {'': ''};
        expect(cubit.fromJson(_json), TextToSpeechStateLoaded.fromMap(_json));
        verifyNever(() => dataModel.setIndexOfActiveSection(any()));
        verifyNever(() => dataModel.setText(any()));
      });

      test('empty json return `null`', () {
        expect(cubit.fromJson({}), null);
        verifyNever(() => dataModel.setIndexOfActiveSection(any()));
        verifyNever(() => dataModel.setText(any()));
      });
    });

    group('toJson (hydration method)', () {
      test('loaded-state returns its map', () {
        expect(cubit.toJson(simplestLoadedState), simplestLoadedState.toMap());
      });

      test('other states return `null`', () {
        expect(
          cubit.toJson(TextToSpeechStateInitial(text: [])),
          null,
        );
        expect(
          cubit.toJson(TextToSpeechStateLoading()),
          null,
        );
      });
    });

    group('cubit init-ed with no actions', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>('initial-state',
          build: () => cubit,
          expect: () => [],
          verify: (_) {
            expect(cubit.state, TextToSpeechStateInitial(text: mockLanguage.explanationText));
            expect(cubit.playPauseAnimationController, null);
            verify(() => textToSpeechInterface.setCompletionCallback(any())).called(1);
            verify(() => textToSpeechInterface.setLanguage(any())).called(1);
            // the following would be called when the completion-callback is triggered
            verifyNever(() => dataModel.indexOfActiveSection);
            verifyNever(() => dataModel.maxPossibleIndex);
            verifyNever(dataModel.toggleIsPlaying);
            verifyNever(() => dataModel.isPlaying);
            verifyNever(dataModel.incrementIndexOfActiveSection);
            verifyNever(() => dataModel.text);
            verifyNever(() => dataModel.percentBeforeActiveSection);
            verifyNever(() => dataModel.activeSection);
            verifyNever(() => textToSpeechInterface.speak(any()));
          });
    });

    blocTest<TextToSpeechCubit, TextToSpeechState>(
      'emitStateLoaded',
      build: () => cubit,
      act: (cubit) => cubit.emitStateLoaded(),
      expect: () => [simplestLoadedState],
      verify: (_) {
        verify(() => dataModel.text).called(1);
        verify(() => dataModel.indexOfActiveSection).called(1);
        verify(() => dataModel.percentBeforeActiveSection).called(1);
        expect(cubit.state, simplestLoadedState);
      },
    );

    blocTest<TextToSpeechCubit, TextToSpeechState>(
      'emitStateLoading',
      build: () => cubit,
      act: (cubit) => cubit.emitStateLoading(),
      expect: () => [TextToSpeechStateLoading()],
      verify: (_) => expect(cubit.state, TextToSpeechStateLoading()),
    );

    group('pasteTextFromClipboard', () {
      group('while _IS NOT_ playing/speaking & clipboard-text _IS NOT_ a URL', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers interface-helpers',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(false);
            when(clipboardInterface.getText).thenAnswer((_) async => pcLoadLetter);
            return cubit;
          },
          act: (cubit) => cubit.pasteTextFromClipboard(),
          expect: () => [
            TextToSpeechStateLoading(),
            simplestLoadedState,
          ],
          verify: (_) {
            // verify(cubit.emitStateLoading).called(1);
            verify(() => dataModel.isPlaying).called(1);
            verifyNever(dataModel.toggleIsPlaying);
            verifyNever(textToSpeechInterface.stop);
            verify(clipboardInterface.getText).called(1);
            verifyNever(() => webUtils.httpGet(any()));
            verify(() => dataModel.setText(pcLoadLetter)).called(1);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });

      group('while _IS_ playing/speaking & clipboard-text _IS_ a URL', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers interface-helpers',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(true);
            when(clipboardInterface.getText).thenAnswer((_) async => ['initech.com']);
            when(() => webUtils.httpGet('initech.com')).thenAnswer((_) async => Response('', 200));
            return cubit;
          },
          act: (cubit) => cubit.pasteTextFromClipboard(),
          expect: () => [
            TextToSpeechStateLoading(),
            simplestLoadedState,
          ],
          verify: (_) {
            // verify(cubit.emitStateLoading).called(1);
            verify(() => dataModel.isPlaying).called(1);
            verify(dataModel.toggleIsPlaying).called(1);
            verify(textToSpeechInterface.stop).called(1);
            verify(clipboardInterface.getText).called(1);
            verify(() => webUtils.httpGet(any())).called(1);
            verify(() => dataModel.setText([
                  'Unable to find header or paragraph text on this website.',
                  '(For nerds: this app captures text from "header" & "paragraph" html-tags.)',
                  'Please check the website by going to it directly to see if this is expected or not.',
                  'If this was not expected, you can email us:',
                  'never.ads.info@gmail.com',
                ])).called(1);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });
    });

    group('reset', () {
      group('while _IS_ playing/speaking', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'calls getters & returns to initial state',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(true);
            when(textToSpeechInterface.stop).thenAnswer((_) async => null);
            return cubit;
          },
          act: (cubit) => cubit.reset(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verify(dataModel.toggleIsPlaying).called(1);
            verify(textToSpeechInterface.stop).called(1);
            verify(dataModel.resetText).called(1);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });

      group('while _IS NOT_ playing/speaking', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'calls getters & returns to initial state',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(false);
            return cubit;
          },
          act: (cubit) => cubit.reset(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verifyNever(dataModel.toggleIsPlaying);
            verifyNever(textToSpeechInterface.stop);
            verify(dataModel.resetText).called(1);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });
    });

    group('play/pause', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'speak when isPlaying == false',
        build: () {
          when(() => dataModel.isPlaying).thenReturn(true);
          when(() => dataModel.activeSection).thenReturn('PC load letter');
          when(() => textToSpeechInterface.speak(any())).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) => cubit.playPause(),
        expect: () => [],
        verify: (_) {
          verify(dataModel.toggleIsPlaying).called(1);
          verify(() => textToSpeechInterface.speak('PC load letter')).called(1);
          verifyNever(textToSpeechInterface.stop);
        },
      );

      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'stop when isPlaying == true',
        build: () {
          when(() => dataModel.isPlaying).thenReturn(false);
          when(textToSpeechInterface.stop).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) => cubit.playPause(),
        expect: () => [],
        verify: (_) {
          verify(dataModel.toggleIsPlaying).called(1);
          verify(textToSpeechInterface.stop).called(1);
          verifyNever(() => textToSpeechInterface.speak(any()));
        },
      );
    });

    group('navigateToSettings', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'when isPlaying is true',
        build: () {
          when(() => dataModel.isPlaying).thenReturn(true);
          when(textToSpeechInterface.stop).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) => cubit.navigateToSettings(),
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.isPlaying).called(1);
          verify(dataModel.toggleIsPlaying).called(1);
          verify(textToSpeechInterface.stop).called(1);
        },
      );

      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'when isPlaying is false',
        build: () {
          when(() => dataModel.isPlaying).thenReturn(false);
          when(textToSpeechInterface.stop).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) => cubit.navigateToSettings(),
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.isPlaying).called(1);
          verifyNever(dataModel.toggleIsPlaying);
          verifyNever(textToSpeechInterface.stop);
        },
      );
    });

    group('navigateFromSettings', () {
      group('when a new language was selected', () {
        setUp(() {
          when(() => hiveStorageInterface.language).thenReturn(Languages.french);
          when(dataModel.getLanguage).thenReturn(Languages.german);
        });

        group('main-screen had explanation-text before nav-ing to settings', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'calls interface-methods',
            build: () => cubit,
            act: (cubit) => cubit.navigateFromSettings(true),
            expect: () => [simplestLoadedState],
            verify: (_) {
              verify(() => hiveStorageInterface.language).called(1);
              verify(() => dataModel.setLanguage(Languages.french)).called(1);
              verify(() => dataModel.setText(Languages.french.explanationText)).called(1);
              verify(cubit.emitStateLoaded).called(1);
            },
          );
        });

        group('main-screen had non-explanation-text before nav-ing to settings', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'calls interface-methods',
            build: () => cubit,
            act: (cubit) => cubit.navigateFromSettings(false),
            expect: () => [],
            verify: (_) {
              verify(() => hiveStorageInterface.language).called(1);
              verify(() => dataModel.setLanguage(Languages.french)).called(1);
              verifyNever(() => dataModel.setText(Languages.french.explanationText));
              verifyNever(cubit.emitStateLoaded);
            },
          );
        });
      });

      group('when a new language was _NOT_ selected', () {
        setUp(() {
          when(() => hiveStorageInterface.language).thenReturn(Languages.french);
          when(dataModel.getLanguage).thenReturn(Languages.french);
        });

        group('main-screen had explanation-text before nav-ing to settings', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'calls interface-methods',
            build: () => cubit,
            act: (cubit) => cubit.navigateFromSettings(true),
            expect: () => [simplestLoadedState],
            verify: (_) {
              verify(() => hiveStorageInterface.language).called(1);
              verifyNever(() => dataModel.setLanguage(any()));
              verify(() => dataModel.setText(Languages.french.explanationText)).called(1);
              verify(cubit.emitStateLoaded).called(1);
            },
          );
        });

        group('main-screen had non-explanation-text before nav-ing to settings', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'calls interface-methods',
            build: () => cubit,
            act: (cubit) => cubit.navigateFromSettings(false),
            expect: () => [],
            verify: (_) {
              verify(() => hiveStorageInterface.language).called(1);
              verifyNever(() => dataModel.setLanguage(Languages.french));
              verifyNever(() => dataModel.setText(Languages.french.explanationText));
              verifyNever(cubit.emitStateLoaded);
            },
          );
        });
      });
    });

    group('goToNextSection', () {
      group('not speaking text', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'index is at max',
          build: () {
            when(() => dataModel.isIndexOfActiveSectionAtMax).thenReturn(true);
            return cubit;
          },
          act: (cubit) async => await cubit.goToNextSection(),
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.isIndexOfActiveSectionAtMax).called(1);
            verifyNever(dataModel.incrementIndexOfActiveSection);
            verifyNever(() => dataModel.isPlaying);
            verifyNever(() => dataModel.activeSection);
            verifyNever(() => textToSpeechInterface.speak(any()));
            verifyNever(() => cubit.emitStateLoaded());
          },
        );

        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'index is not yet at max',
          build: () {
            when(() => dataModel.isIndexOfActiveSectionAtMax).thenReturn(false);
            when(() => dataModel.isPlaying).thenReturn(false);
            return cubit;
          },
          act: (cubit) async => await cubit.goToNextSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isIndexOfActiveSectionAtMax).called(1);
            verify(dataModel.incrementIndexOfActiveSection).called(1);
            verify(() => dataModel.isPlaying).called(1);
            verifyNever(() => dataModel.activeSection);
            verifyNever(() => textToSpeechInterface.speak(any()));
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });

      group('when speaking text', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'index is not at max',
          build: () {
            when(() => dataModel.isIndexOfActiveSectionAtMax).thenReturn(false);
            when(() => dataModel.isPlaying).thenReturn(true);
            when(() => dataModel.activeSection).thenReturn('');
            when(() => textToSpeechInterface.speak('')).thenAnswer((_) async => null);
            return cubit;
          },
          act: (cubit) async => await cubit.goToNextSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isIndexOfActiveSectionAtMax).called(1);
            verify(dataModel.incrementIndexOfActiveSection).called(1);
            verify(() => dataModel.isPlaying).called(1);
            verify(() => dataModel.activeSection).called(1);
            verify(() => textToSpeechInterface.speak(any())).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });
    });

    group('goToPreviousSection', () {
      group('first section is active', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          '_DO NOT_ trigger other methods',
          build: () {
            when(() => dataModel.indexOfActiveSection).thenReturn(0);
            return cubit;
          },
          act: (cubit) async => await cubit.goToPreviousSection(),
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.indexOfActiveSection).called(1);
            verifyNever(dataModel.decrementIndexOfActiveSection);
            verifyNever(() => textToSpeechInterface.speak(any()));
            verifyNever(cubit.emitStateLoaded);
          },
        );
      });

      group('a later section is active (not the first section)', () {
        group('while speaking text', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'trigger other methods',
            build: () {
              when(() => dataModel.indexOfActiveSection).thenReturn(1);
              when(() => dataModel.activeSection).thenReturn('');
              when(() => dataModel.isPlaying).thenReturn(true);
              when(() => textToSpeechInterface.speak(any())).thenAnswer((_) async => null);
              return cubit;
            },
            act: (cubit) async => await cubit.goToPreviousSection(),
            expect: () => [
              TextToSpeechStateLoaded(text: [], indexOfActiveSection: 1, percentComplete: 0),
            ],
            verify: (_) {
              verify(() => dataModel.indexOfActiveSection).called(2);
              verify(dataModel.decrementIndexOfActiveSection).called(1);
              verify(() => dataModel.isPlaying).called(1);
              verify(() => dataModel.activeSection).called(1);
              verify(() => textToSpeechInterface.speak(any())).called(1);
              verify(cubit.emitStateLoaded).called(1);
            },
          );
        });

        group('while _NOT_ speaking text', () {
          blocTest<TextToSpeechCubit, TextToSpeechState>(
            'trigger other methods',
            build: () {
              when(() => dataModel.indexOfActiveSection).thenReturn(1);
              when(() => dataModel.isPlaying).thenReturn(false);
              return cubit;
            },
            act: (cubit) async => await cubit.goToPreviousSection(),
            expect: () => [
              TextToSpeechStateLoaded(text: [], indexOfActiveSection: 1, percentComplete: 0),
            ],
            verify: (_) {
              verify(() => dataModel.indexOfActiveSection).called(2);
              verify(dataModel.decrementIndexOfActiveSection).called(1);
              verify(() => dataModel.isPlaying).called(1);
              verifyNever(() => dataModel.activeSection);
              verifyNever(() => textToSpeechInterface.speak(any()));
              verify(cubit.emitStateLoaded).called(1);
            },
          );
        });
      });
    });

    group('skip-to-end button', () {
      group('is paused/stopped', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers other methods',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(false);
            return cubit;
          },
          act: (cubit) => cubit.skipToLastSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verifyNever(textToSpeechInterface.stop);
            verifyNever(dataModel.toggleIsPlaying);
            verify(dataModel.setIndexOfActiveSectionToMax).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });

      group('is speaking', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers other methods',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(true);
            return cubit;
          },
          act: (cubit) => cubit.skipToLastSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verify(textToSpeechInterface.stop).called(1);
            verify(dataModel.toggleIsPlaying).called(1);
            verify(dataModel.setIndexOfActiveSectionToMax).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });
    });

    group('skip-to-start button', () {
      group('is paused/stopped', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers other methods',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(false);
            return cubit;
          },
          act: (cubit) => cubit.skipToFirstSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verifyNever(textToSpeechInterface.stop);
            verifyNever(dataModel.toggleIsPlaying);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });

      group('is speaking', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers other methods',
          build: () {
            when(() => dataModel.isPlaying).thenReturn(true);
            return cubit;
          },
          act: (cubit) => cubit.skipToFirstSection(),
          expect: () => [simplestLoadedState],
          verify: (_) {
            verify(() => dataModel.isPlaying).called(1);
            verify(textToSpeechInterface.stop).called(1);
            verify(dataModel.toggleIsPlaying).called(1);
            verify(dataModel.resetIndexOfActiveSection).called(1);
            verify(cubit.emitStateLoaded).called(1);
          },
        );
      });
    });

    group('setPitchFromStorage', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers `pitch` (getter) from storage & sets it on tts',
        build: () {
          when(() => hiveStorageInterface.pitch).thenReturn(0.9);
          when(() => textToSpeechInterface.setPitch(0.9)).thenAnswer((_) async => await null);
          return cubit;
        },
        act: (cubit) => cubit.setPitchFromStorage(),
        expect: () => [],
        verify: (_) {
          verify(() => hiveStorageInterface.pitch).called(1);
          verify(() => textToSpeechInterface.setPitch(0.9)).called(1);
        },
      );
    });

    group('setSpeechRateFromStorage', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers `speechRate` (getter) from storage & sets it on tts',
        build: () {
          when(() => hiveStorageInterface.speechRate).thenReturn(0.42);
          when(() => textToSpeechInterface.setSpeechRate(0.42)).thenAnswer((_) async => await null);
          return cubit;
        },
        act: (cubit) => cubit.setSpeechRateFromStorage(),
        expect: () => [],
        verify: (_) {
          verify(() => hiveStorageInterface.speechRate).called(1);
          verify(() => textToSpeechInterface.setSpeechRate(0.42)).called(1);
        },
      );
    });

    group('indexOfActiveSection (getter)', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers getter from data-model',
        build: () {
          when(() => dataModel.indexOfActiveSection).thenReturn(9);
          return cubit;
        },
        act: (cubit) => cubit.indexOfActiveSection,
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.indexOfActiveSection).called(1);
        },
      );
    });

    group('maxPossibleIndex (getter)', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers getter from data-model',
        build: () {
          when(() => dataModel.maxPossibleIndex).thenReturn(42);
          return cubit;
        },
        act: (cubit) => cubit.maxPossibleIndex,
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.maxPossibleIndex).called(1);
        },
      );
    });

    group('isAppInForeground (getter)', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers getter from data-model',
        build: () {
          when(() => dataModel.isAppInForeground).thenReturn(true);
          return cubit;
        },
        act: (cubit) => cubit.isAppInForeground,
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.isAppInForeground).called(1);
          expect(cubit.isAppInForeground, true);
        },
      );
    });

    group('setIsAppInForeground', () {
      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers setter from data-model',
        build: () {
          when(() => dataModel.setIsAppInForeground(false)).thenReturn(null);
          return cubit;
        },
        act: (cubit) => cubit.setIsAppInForeground(false),
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.setIsAppInForeground(false)).called(1);
        },
      );
    });

    group('getDurationFromSizeOfSectionAtIndex', () {
      late Duration duration;

      setUp(() {
        when(() => dataModel.text).thenReturn(['PC', 'load letter ' * 20]);
      });

      group('for section_0 (tiny section)', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers getter from data-model & computes duration-value',
          build: () => cubit,
          act: (cubit) => duration = cubit.getScrollDurationForSectionAtIndex(0),
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.text).called(1);
            expect(duration, Duration(milliseconds: 151));
          },
        );
      });

      group('for section_1 (typical paragraph-length)', () {
        blocTest<TextToSpeechCubit, TextToSpeechState>(
          'triggers getter from data-model & computes duration-value',
          build: () => cubit,
          act: (cubit) => duration = cubit.getScrollDurationForSectionAtIndex(1),
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.text).called(1);
            expect(duration, Duration(milliseconds: 270));
          },
        );
      });
    });

    group('percentInActiveSection', () {
      late int percent;

      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'triggers getter from data-model',
        build: () {
          when(() => dataModel.percentInActiveSection).thenReturn(9);
          return cubit;
        },
        act: (cubit) => percent = cubit.percentInActiveSection,
        expect: () => [],
        verify: (_) {
          verify(() => dataModel.percentInActiveSection).called(1);
          expect(percent, 9);
        },
      );
    });

    group('getLanguage', () {
      late Language language;

      blocTest<TextToSpeechCubit, TextToSpeechState>(
        'gets from data-model',
        build: () {
          when(() => hiveStorageInterface.language).thenReturn(Languages.french);
          return cubit;
        },
        act: (cubit) => language = cubit.getLanguage(),
        expect: () => [],
        verify: (_) {
          verify(() => hiveStorageInterface.language).called(1);
          expect(language, Languages.french);
        },
      );
    });

    group('isExplanationText', () {
      group('when data-model text _IS_ explanation text', () {
        late final bool result;

        blocTest<TextToSpeechCubit, TextToSpeechState>(
          "checks against all languages' explanation-texts to see if data-model-text is a match",
          build: () {
            when(() => dataModel.text).thenReturn(Languages.english.explanationText);
            return cubit;
          },
          act: (cubit) => result = cubit.isExplanationText,
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.text).called(1);
            expect(result, true);
          },
        );
      });

      group('when data-model text _IS NOT_ explanation text', () {
        late final bool result;

        blocTest<TextToSpeechCubit, TextToSpeechState>(
          "checks against all languages' explanation-texts to see if data-model-text is a match",
          build: () {
            when(() => dataModel.text).thenReturn(['PC', 'load letter']);
            return cubit;
          },
          act: (cubit) => result = cubit.isExplanationText,
          expect: () => [],
          verify: (_) {
            verify(() => dataModel.text).called(1);
            expect(result, false);
          },
        );
      });
    });
  });
}
