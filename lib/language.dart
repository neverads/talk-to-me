import "package:equatable/equatable.dart";

class Language extends Equatable {
  const Language({
    required this.locale,
    required this.flag,
    required this.explanationText,
    required this.sampleText,
    required this.jumpToStartText,
    required this.jumpToEndText,
  });

  factory Language.fromLocale(String flag) {
    if (flag == Languages.english.locale) {
      return Languages.english;
    } else if (flag == Languages.spanish.locale) {
      return Languages.spanish;
    } else if (flag == Languages.french.locale) {
      return Languages.french;
    } else if (flag == Languages.german.locale) {
      return Languages.german;
    } else {
      return Languages.english;
    }
  }

  final String locale;
  final String flag;
  final List<String> explanationText;
  final String sampleText;
  final String jumpToStartText;
  final String jumpToEndText;

  String toString() {
    return 'Language { locale: "$locale", flag: "$flag" }';
  }

  @override
  List<Object?> get props => [locale, flag];
}

class Languages {
  // This class is not meant to be instantiated or extended; this constructor
  // prevents instantiation and extension.
  const Languages._();

  static const List<Language> allSupported = const [english, spanish, french, german];

  static const Language english = const Language(
    locale: "en-US",
    flag: "🇺🇸",
    explanationText: const [
      "Paste text by tapping the clipboard-button above.",
      "If you paste a URL (a link to a website), the text from that web-page will be displayed.",
      "Never Ads is supported entirely by donations; all our apps are free & open-source with no ads or data-collection.",
    ],
    sampleText: "How does this sound?  Use the sliders to adjust the rate and pitch of the voice.",
    jumpToStartText: "Long-press to jump to the start.",
    jumpToEndText: "Long-press to jump to the end.",
  );

  static const Language spanish = const Language(
    locale: "es",
    flag: "🇪🇸",
    explanationText: const [
      "Pega el texto pulsando el botón de la tabilla con sujetapapeles.",
      "Si pegas aquí la página web, el texto de la página estará cargado.",
      "Nuestro código es de código abierto en gitlab.com/neverads.",
    ],
    sampleText: "¿Cómo suena esto?  Utiliza los controles deslizantes para ajustar el ritmo y el tono de la voz.",
    jumpToStartText: "Pulsa prolongadamente para saltar al inicio.",
    jumpToEndText: "Pulsa prolongadamente para saltar al final.",
  );

  static const Language french = const Language(
    locale: "fr",
    flag: "🇫🇷",
    explanationText: const [
      "Collez du texte en pressant l'icone porte-bloc.",
      "Le texte d'une page internet s'affichera en collant son adresse ici.",
      "Notre code est open-source sur gitlab.com/neverads.",
    ],
    sampleText: "Comment cela sonne-t-il ?  Utilisez les curseurs pour régler le rythme et la hauteur de la voix.",
    jumpToStartText: "Appuyez longuement pour sauter au début.",
    jumpToEndText: "Appuyez longuement pour aller à la fin.",
  );

  static const Language german = const Language(
    locale: "de",
    flag: "🇩🇪",
    explanationText: const [
      "Füge Text ein indem du auf den Zwischenablagebutton drügst.",
      "Wenn du eine Web-Adresse einfügst, wird der Text der Website angezeigt.",
      "Unser Code ist Open-Source unter gitlab.com/neverads.",
    ],
    sampleText:
        "Wie klingt das?  Verwenden Sie die Schieberegler, um die Geschwindigkeit und die Tonhöhe der Stimme einzustellen.",
    jumpToStartText: "Drücken Sie lange, um zum Start zu springen.",
    jumpToEndText: "Drücken Sie lange, um zum Ende zu springen.",
  );
}
