part of 'text_to_speech_cubit.dart';

@immutable
abstract class TextToSpeechState extends Equatable {
  const TextToSpeechState();

  @override
  String toString() => this.runtimeType.toString();

  @override
  List<Object?> get props => [this.toString()];

  Map<String, dynamic> toMap() => {};
}

class TextToSpeechStateLoading extends TextToSpeechState {
  const TextToSpeechStateLoading();
}

class TextToSpeechStateLoaded extends TextToSpeechState {
  final Iterable<String> text;
  final int indexOfActiveSection;
  final int percentComplete;

  const TextToSpeechStateLoaded({
    required this.text,
    required this.indexOfActiveSection,
    required this.percentComplete,
  });

  @override
  List<Object?> get props => [text, indexOfActiveSection, percentComplete];

  @override
  Map<String, dynamic> toMap() => {
        'text': text.toList(),
        'indexOfActiveSection': indexOfActiveSection,
        'percentComplete': percentComplete,
      };

  factory TextToSpeechStateLoaded.fromMap(Map<String, dynamic> map) {
    final List<dynamic> _text = map['text'] ?? Languages.english.explanationText;
    return TextToSpeechStateLoaded(
      text: _text.map((e) => e.toString()).toList(),
      indexOfActiveSection: map['indexOfActiveSection'] ?? 0,
      percentComplete: map['percentComplete'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory TextToSpeechStateLoaded.fromJson(String source) => TextToSpeechStateLoaded.fromMap(json.decode(source));

  @override
  String toString() {
    final String type = this.runtimeType.toString();
    final String _text = text.isEmpty ? '[]' : '["${text.join('", "')}"]';
    return '$type(text: $_text, indexOfActiveSection: $indexOfActiveSection, percentComplete: $percentComplete)';
  }
}

class TextToSpeechStateInitial extends TextToSpeechStateLoaded {
  final Iterable<String> text;

  const TextToSpeechStateInitial({required this.text})
      : super(
          text: text,
          indexOfActiveSection: 0,
          percentComplete: 0,
        );
}
