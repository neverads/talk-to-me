import 'package:html/dom.dart';
import 'package:http/http.dart';
import 'package:talk_to_me/interfaces/web_utils.dart';
import 'package:test/test.dart';

void main() {
  group('WebUtils', () {
    late WebUtils webUtils;

    setUp(() {
      webUtils = WebUtils();
    });

    group('http.get', () {
      test('good response', () async {
        final response = await webUtils.httpGet("https://example.com");
        expect(response.request?.url.toString(), "https://example.com");
        expect(response.statusCode, 200);
      });

      test('bad response', () {
        expect(() => webUtils.httpGet("https://bad_url.com/never_gonna_work"), throwsException);
      });
    });
  });

  group('Extensions', () {
    group('WebsiteParser', () {
      group('urlOrPlaceholder', () {
        test('returns URL (from good request)', () {
          expect(
            Response('<p>body</p>', 200, request: Request('GET', Uri.parse('example.com'))).urlOrPlaceholder,
            '"example.com"',
          );
        });

        test('returns placeholder (from null-request)', () {
          expect(
            Response('<p>body</p>', 200).urlOrPlaceholder,
            'this website',
          );
        });
      });

      group('texts', () {
        test('with successful request', () {
          expect(Response('<p>body</p>', 200).texts, ['body']);
        });
        test('with empty-body', () {
          expect(
            Response('', 200).texts,
            [
              'Unable to find header or paragraph text on this website.',
              '(For nerds: this app captures text from "header" & "paragraph" html-tags.)',
              'Please check the website by going to it directly to see if this is expected or not.',
              'If this was not expected, you can email us:',
              'never.ads.info@gmail.com',
            ],
          );
        });
        test('with failed request', () {
          expect(
            Response('', 404).texts,
            [
              'Unable to connect to this website.',
              'The website is not accepting our attempt to connect to it.'
                  'We expect them to tell us that everything is okay via a "200" code, '
                  'but they sent a "404" instead.',
              'A "404"/"400"/etc means that this website does not exist or cannot be accessed.',
              'Please try again, and if this persists, email us:',
              'never.ads.info@gmail.com',
            ],
          );
        });
      });
    });

    group('UrlChecker', () {
      test('isUrl', () {
        expect([].isUrl, false);
        expect([null].isUrl, false);
        expect([42].isUrl, false);
        expect([3.14].isUrl, false);
        expect([""].isUrl, false);
        expect(["PC", "load letter"].isUrl, false);
        expect(["PC load letter"].isUrl, false);
        expect(["initech.com"].isUrl, true);
        expect(["www.initech.com"].isUrl, true);
        expect(["http://www.initech.com"].isUrl, true);
        expect(["https://www.initech.com"].isUrl, true);
        expect(["http://initech.com"].isUrl, true);
        expect(["https://initech.com"].isUrl, true);
      });
    });

    group('TextExtractor', () {
      test('texts', () {
        expect(Document.html('<p>PC load letter</p>').texts, ['PC load letter']);
        expect(Document.html('<h1>PC</h1><h2>load letter</h2>').texts, ['PC', 'load letter']);
        expect(Document.html('<h2>PC</h2><h6>load letter</h6>').texts, ['PC', 'load letter']);
        expect(Document.html('<h4>PC</h4><h5>load letter</h5>').texts, ['PC', 'load letter']);
        expect(Document.html('<div>PC load letter</div>').texts, []);
      });
    });
  });
}
