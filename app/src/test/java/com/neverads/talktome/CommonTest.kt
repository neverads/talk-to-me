package com.neverads.talktome

import org.junit.Test
import org.junit.Assert.*

data class Parameter(val index: Int,
                     val isOptional: Boolean = false,
                     val isVararg: Boolean = false,
                     val name: String?,
                     val type: String)

class GetMaxTextLength {
    @Test
    fun hasName() {
        assertEquals("getMaxTextLength", ::getMaxTextLength.name)
    }

    @Test
    fun hasParameters() {
        val actualParams = ::getMaxTextLength.parameters
        assertEquals(0, actualParams.size)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Int", ::getMaxTextLength.returnType.toString())
    }
}

class GetPercentsComplete {
    @Test
    fun hasName() {
        assertEquals("getPercentsComplete", ::getPercentsComplete.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
                Parameter(index = 0, name = "text", type = "kotlin.CharSequence"),
                Parameter(index = 1, name = "activeSectionOfText", type = "kotlin.String"))
        val actualParams = mutableListOf<Parameter>()
        ::getPercentsComplete.parameters.forEach {
            actualParams.add(Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Pair<kotlin.Int, kotlin.Int>", ::getPercentsComplete.returnType.toString())
    }

    @Test
    fun usageEmptyStrings() {
        assertEquals(Pair(0, 1), getPercentsComplete(text = "", activeSectionOfText = ""))
    }

    @Test
    fun usagePredictableStrings() {
        val hundredChars = "i23456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789_"
        assertEquals(100, hundredChars.length)
        val char0 = hundredChars[0]
        val charLast = hundredChars[hundredChars.length - 1]
        assertEquals('i', char0)
        assertEquals('_', charLast)
        assertEquals(Pair(0, 2), getPercentsComplete(text = hundredChars, activeSectionOfText = char0.toString()))
        assertEquals(Pair(99, 100), getPercentsComplete(text = hundredChars, activeSectionOfText = charLast.toString()))

        val thousandChars = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789_i23456789o"
        assertEquals(1000, thousandChars.length)
        val char989 = thousandChars[989]
        val char990 = thousandChars[990]
        val char999 = thousandChars[999]
        assertEquals('_', char989)
        assertEquals('i', char990)
        assertEquals('o', char999)
        assertEquals(Pair(98, 100), getPercentsComplete(text = thousandChars, activeSectionOfText = char989.toString()))
        assertEquals(Pair(99, 100), getPercentsComplete(text = thousandChars, activeSectionOfText = char990.toString()))
        assertEquals(Pair(99, 100), getPercentsComplete(text = thousandChars, activeSectionOfText = char999.toString()))
    }

    @Test
    fun usagePracticalStrings() {
        val practicalText = "Paste text by long-pressing here.\n\nIf you paste a web-address, the text from that web-page will be displayed.\n\nFrom other apps, you can \"Share\" a web-address or other text to directly load it here."
        val (paragraph0, paragraph1, paragraph2) = practicalText.split("\n\n")
        assertEquals(Pair(0, 18), getPercentsComplete(text = practicalText, activeSectionOfText = paragraph0))
        assertEquals(Pair(18, 56), getPercentsComplete(text = practicalText, activeSectionOfText = paragraph1))
        assertEquals(Pair(56, 100), getPercentsComplete(text = practicalText, activeSectionOfText = paragraph2))
    }
}

class GetSpeechPitchFromProgress {
    @Test
    fun hasName() {
        assertEquals("getSpeechPitchFromProgress", ::getSpeechPitchFromProgress.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(Parameter(index = 0, name = "progress", type = "kotlin.Int"))
        val actualParams = mutableListOf<Parameter>()
        ::getSpeechPitchFromProgress.parameters.forEach {
            actualParams.add(Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Float", ::getSpeechPitchFromProgress.returnType.toString())
    }

    @Test
    fun usageMinValues() {
        assertEquals(0.7f, getSpeechPitchFromProgress(progress = 0), 0.001F)
        assertEquals(0.74f, getSpeechPitchFromProgress(progress = 1), 0.001F)
    }

    @Test
    fun usageMaxValues() {
        val realMaxValue = 14
        val ridiculousValue = 100
        assertEquals(1.26f, getSpeechPitchFromProgress(progress = realMaxValue), 0.001F)
        assertEquals(4.7f, getSpeechPitchFromProgress(progress = ridiculousValue), 0.001F)
    }

    @Test
    fun usageNormalValues() {
        assertEquals(1.18f, getSpeechPitchFromProgress(progress = 12), 0.001F)
        assertEquals(0.98f, getSpeechPitchFromProgress(progress = 7), 0.001F)
        assertEquals(0.78f, getSpeechPitchFromProgress(progress = 2), 0.001F)
    }
}

class GetSpeechRateFromProgress {
    @Test
    fun hasName() {
        assertEquals("getSpeechRateFromProgress", ::getSpeechRateFromProgress.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(
                Parameter(index = 0, name = "progress", type = "kotlin.Int"))
        val actualParams = mutableListOf<Parameter>()
        ::getSpeechRateFromProgress.parameters.forEach {
            actualParams.add(Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Float", ::getSpeechRateFromProgress.returnType.toString())
    }

    @Test
    fun usageMinValues() {
        assertEquals(0.6f, getSpeechRateFromProgress(progress = 0), 0.001F)
        assertEquals(0.73f, getSpeechRateFromProgress(progress = 1), 0.001F)
    }

    @Test
    fun usageMaxValues() {
        val realMaxValue = 15
        val ridiculousValue = 100
        assertEquals(2.55f, getSpeechRateFromProgress(progress = realMaxValue), 0.001F)
        assertEquals(13.6f, getSpeechRateFromProgress(progress = ridiculousValue), 0.001F)
    }

    @Test
    fun usageNormalValues() {
        assertEquals(2.16f, getSpeechRateFromProgress(progress = 12), 0.001F)
        assertEquals(1.51f, getSpeechRateFromProgress(progress = 7), 0.001F)
        assertEquals(0.86f, getSpeechRateFromProgress(progress = 2), 0.001F)
    }
}

class GetUrlStringFromText {
    @Test
    fun hasName() {
        assertEquals("getUrlStringFromText", ::getUrlStringFromText.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = listOf(Parameter(index = 0, name = "urlAsText", type = "kotlin.CharSequence"))
        val actualParams = mutableListOf<Parameter>()
        ::getUrlStringFromText.parameters.forEach {
            actualParams.add(Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.String", ::getUrlStringFromText.returnType.toString())
    }

    @Test
    fun usageHttpPrefix() {
        assertEquals("http://", getUrlStringFromText(urlAsText = "http://"))
        assertEquals("http://www.never-ads.com", getUrlStringFromText(urlAsText = "http://www.never-ads.com"))
        assertEquals("http://never-ads.com", getUrlStringFromText(urlAsText = "http://never-ads.com"))
    }

    @Test
    fun usageHttpsPrefix() {
        assertEquals("https://", getUrlStringFromText(urlAsText = "https://"))
        assertEquals("https://www.never-ads.com", getUrlStringFromText(urlAsText = "https://www.never-ads.com"))
        assertEquals("https://never-ads.com", getUrlStringFromText(urlAsText = "https://never-ads.com"))
    }

    @Test
    fun usageWithoutPrefix() {
        assertEquals("https://", getUrlStringFromText(urlAsText = ""))
        assertEquals("https://www.never-ads.com", getUrlStringFromText(urlAsText = "www.never-ads.com"))
        assertEquals("https://never-ads.com", getUrlStringFromText(urlAsText = "never-ads.com"))
    }
}

class IsTextUrl {
    @Test
    fun hasName() {
        assertEquals("isUrl", String::isUrl.name)
    }

    @Test
    fun hasParameters() {
        val expectedParams = emptyList<Parameter>()
        val actualParams = mutableListOf<Parameter>()
        "test string"::isUrl.parameters.forEach {
            actualParams.add(Parameter(
                    index = it.index,
                    isOptional = it.isOptional,
                    isVararg = it.isVararg,
                    name = it.name,
                    type = it.type.toString()))
        }
        assertEquals(expectedParams.size, actualParams.size)
        assertEquals(expectedParams, actualParams)
    }

    @Test
    fun returnType() {
        assertEquals("kotlin.Boolean", String::isUrl.returnType.toString())
    }
}
