import 'package:hive/hive.dart';
import 'package:mocktail/mocktail.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:test/test.dart';

class MockHive extends Mock implements HiveInterface {}

void main() {
  group('HiveInterface', () {
    late HiveInterface hive;
    late HiveStorageInterface hiveInterface;

    setUp(() {
      hive = MockHive();
      hiveInterface = HiveStorageInterface(hive: hive);
    });

    group('initial state', () {
      test('constructor instantiates internal Hive when not injected', () {
        expect(HiveStorageInterface(), isNotNull);
      });
      test('constants', () {
        expect(HiveStorageInterface.generalStorageBoxName, 'box_name_general');
        expect(HiveStorageInterface.generalStorageBoxKeyLanguageFlag, '__generalStorageBoxKeyLanguageFlag');
        expect(HiveStorageInterface.generalStorageBoxKeyPitch, '__generalStorageBoxKeyPitch');
        expect(HiveStorageInterface.generalStorageBoxKeySpeechRate, "__generalStorageBoxKeySpeechRate");
      });
    });

    group('openStorageBox', () {
      test('calls openBox', () async {
        try {
          await hiveInterface.openStorageBox();
        } catch (_) {}
        verify(() => hive.openBox(HiveStorageInterface.generalStorageBoxName)).called(1);
      });
    });

    group('storageBox', () {
      test('calls box (gets an opened box - not data inside)', () async {
        try {
          hiveInterface.storageBox;
        } catch (_) {}
        verify(() => hive.box(HiveStorageInterface.generalStorageBoxName)).called(1);
      });
    });
  });
}
