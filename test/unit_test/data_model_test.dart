import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/language.dart';
import 'package:test/test.dart';

void main() {
  group('DataModel class', () {
    test('initial state', () {
      DataModel dataModel = DataModel(language: Languages.english);
      expect(dataModel.isAppInForeground, true);
      expect(dataModel.isPlaying, false);
      expect(dataModel.language, Languages.english);
      expect(dataModel.text, Languages.english.explanationText);
      expect(dataModel.indexOfActiveSection, 0);
      expect(dataModel.activeSection, Languages.english.explanationText[0]);
      expect(dataModel.maxPossibleIndex, 2);
      expect(dataModel.totalCharsInText, 250);
      expect(dataModel.percentBeforeActiveSection, 0);
      expect(dataModel.isIndexOfActiveSectionAtMax, false);
    });

    test('can change section-index, impacting getters', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 1);
      expect(dataModel.activeSection, Languages.english.explanationText[1]);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 2);
      expect(dataModel.isIndexOfActiveSectionAtMax, true);
      expect(dataModel.activeSection, Languages.english.explanationText[2]);
      dataModel.decrementIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 1);
      expect(dataModel.activeSection, Languages.english.explanationText[1]);
      dataModel.resetIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 0);
      expect(dataModel.activeSection, Languages.english.explanationText[0]);
      dataModel.setIndexOfActiveSectionToMax();
      expect(dataModel.isIndexOfActiveSectionAtMax, true);
      expect(dataModel.activeSection, Languages.english.explanationText[2]);
      dataModel.setIndexOfActiveSection(1);
      expect(dataModel.isIndexOfActiveSectionAtMax, false);
      expect(dataModel.activeSection, Languages.english.explanationText[1]);
    });

    test('cannot change section-index beyond text-indices', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.incrementIndexOfActiveSection();
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 2);
      expect(() => dataModel.incrementIndexOfActiveSection(), throwsA(CannotIncrementError(2, 2)));
      expect(dataModel.indexOfActiveSection, 2);
      dataModel.decrementIndexOfActiveSection();
      dataModel.decrementIndexOfActiveSection();
      expect(dataModel.indexOfActiveSection, 0);
      expect(() => dataModel.decrementIndexOfActiveSection(), throwsA(CannotDecrementError()));
      expect(dataModel.indexOfActiveSection, 0);
    });

    test('can set new text, impacting getters', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.setText(["PC", "load letter"]);
      expect(dataModel.text, ["PC", "load letter"]);
      expect(dataModel.maxPossibleIndex, 1);
      expect(dataModel.totalCharsInText, 13);
    });

    test('can reset text, impacting text-getter', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.setText(["PC", "load letter"]);
      expect(dataModel.text, ["PC", "load letter"]);
      dataModel.resetText();
      expect(dataModel.text, Languages.english.explanationText);
    });

    test('can get the % of text which is in the active section', () {
      DataModel dataModel = DataModel(language: Languages.english);
      expect(dataModel.percentInActiveSection, 20);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.percentInActiveSection, 35);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.percentInActiveSection, 45);
    });

    test('can get the % of text which is before the active section', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.percentBeforeActiveSection, 20);
      dataModel.incrementIndexOfActiveSection();
      expect(dataModel.percentBeforeActiveSection, 55);
    });

    test('calling toggleIsPlaying affects isPlaying', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.toggleIsPlaying();
      expect(dataModel.isPlaying, true);
      dataModel.toggleIsPlaying();
      expect(dataModel.isPlaying, false);
    });

    test('calling setIsAppInForeground affects variable', () {
      DataModel dataModel = DataModel(language: Languages.english);
      expect(dataModel.isAppInForeground, true);
      dataModel.setIsAppInForeground(false);
      expect(dataModel.isAppInForeground, false);
      dataModel.setIsAppInForeground(true);
      expect(dataModel.isAppInForeground, true);
    });

    test('calling setLanguage affects variable', () {
      DataModel dataModel = DataModel(language: Languages.english);
      dataModel.setLanguage(Languages.german);
      expect(dataModel.language, Languages.german);
      dataModel.setLanguage(Languages.spanish);
      expect(dataModel.language, Languages.spanish);
    });

    test('calling getLanguage is same as variable', () {
      DataModel dataModel = DataModel(language: Languages.french);
      expect(dataModel.getLanguage(), dataModel.language);
    });
  });
}
