# Talk to Me

by [never ads](https://play.google.com/store/apps/developer?id=Never+Ads)

_No ads, no data-collection, no cost.  Ever.  And we're [open-source](gitlab.com/neverads)!_  

Text-to-speech app.  While on the go, listen to an article, a blog post, or whatever else your heart desires.
