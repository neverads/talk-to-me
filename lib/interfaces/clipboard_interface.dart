import 'package:flutter/services.dart';

class ClipboardInterface {
  const ClipboardInterface();

  Future<Iterable<String>> getText() async {
    final data = await Clipboard.getData(Clipboard.kTextPlain);
    final text = data?.text ?? "No text is copied to your clipboard.";
    return text.split("\n").map((it) => it.trim()).where((it) => it.isNotEmpty);
  }
}
