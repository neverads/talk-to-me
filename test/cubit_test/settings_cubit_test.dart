import 'package:bloc_test/bloc_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:talk_to_me/cubit/settings_cubit.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/language.dart';
import 'package:test/test.dart';

class MockTextToSpeechInterface extends Mock implements TextToSpeechInterface {}

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class MockDataModel extends Mock implements DataModel {
  Language language = Languages.english;
}

void main() {
  group('SettingsCubit', () {
    late TextToSpeechInterface textToSpeechInterface;
    late HiveStorageInterface hiveStorageInterface;
    late DataModel dataModel;
    late SettingsCubit cubit;

    setUp(() {
      textToSpeechInterface = MockTextToSpeechInterface();
      hiveStorageInterface = MockHiveStorageInterface();
      dataModel = MockDataModel();
      cubit = SettingsCubit(
        textToSpeechInterface,
        hiveStorageInterface,
        dataModel,
      );
      when(textToSpeechInterface.stop).thenAnswer((_) async => null);
    });

    tearDown(() => cubit.close());

    blocTest<SettingsCubit, SettingsState>(
      'no actions',
      build: () => cubit,
      expect: () => [],
      verify: (_) {
        expect(cubit.state, SettingsState(language: Languages.english));
        verify(textToSpeechInterface.clearCompletionCallback).called(1);

        // triggered in tearDown()/close()
        verify(textToSpeechInterface.stop).called(1);
        verify(textToSpeechInterface.restoreCompletionCallback).called(1);
      },
    );

    group('get pitch', () {
      blocTest<SettingsCubit, SettingsState>(
        'uses hiveInterface.pitch',
        build: () {
          when(() => hiveStorageInterface.pitch).thenReturn(0.0);
          return cubit;
        },
        act: (cubit) => cubit.pitch,
        expect: () => [],
        verify: (_) {
          verify(() => hiveStorageInterface.pitch).called(1);
        },
      );
    });

    group('get speech-rate', () {
      blocTest<SettingsCubit, SettingsState>(
        'uses hiveInterface.speechRate',
        build: () {
          when(() => hiveStorageInterface.speechRate).thenReturn(0.0);
          return cubit;
        },
        act: (cubit) => cubit.speechRate,
        expect: () => [],
        verify: (_) => verify(() => hiveStorageInterface.speechRate).called(1),
      );
    });

    group('stopSpeaking', () {
      blocTest<SettingsCubit, SettingsState>(
        'calls textToSpeechInterface.stop',
        build: () {
          when(textToSpeechInterface.stop).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) async => await cubit.stopSpeaking(),
        expect: () => [],
        verify: (_) => verify(textToSpeechInterface.stop).called(2), // +1 during `cubit.close()`
      );
    });

    group('speakSampleText', () {
      blocTest<SettingsCubit, SettingsState>(
        'calls speaks without continuing to a next section',
        build: () {
          when(dataModel.getLanguage).thenReturn(Languages.spanish);
          when(() => textToSpeechInterface.speak(Languages.spanish.sampleText)).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) async => await cubit.speakSampleText(),
        expect: () => [],
        verify: (_) {
          verify(dataModel.getLanguage).called(1);
          verify(() => textToSpeechInterface.speak(Languages.spanish.sampleText)).called(1);
        },
      );
    });

    group('setSpeechRate', () {
      blocTest<SettingsCubit, SettingsState>(
        'sets speech-rate for tts & stores value in storage',
        build: () {
          when(() => textToSpeechInterface.setSpeechRate(0.42)).thenAnswer((_) async => null);
          when(() => hiveStorageInterface.setSpeechRate(0.42)).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) async => await cubit.setSpeechRate(0.42),
        expect: () => [],
        verify: (_) {
          verify(() => textToSpeechInterface.setSpeechRate(0.42)).called(1);
          verify(() => hiveStorageInterface.setSpeechRate(0.42)).called(1);
        },
      );
    });

    group('setPitch', () {
      blocTest<SettingsCubit, SettingsState>(
        'sets pitch for tts & stores value in storage',
        build: () {
          when(() => textToSpeechInterface.setPitch(0.42)).thenAnswer((_) async => null);
          when(() => hiveStorageInterface.setPitch(0.42)).thenAnswer((_) async => null);
          return cubit;
        },
        act: (cubit) async => await cubit.setPitch(0.42),
        expect: () => [],
        verify: (_) {
          verify(() => textToSpeechInterface.setPitch(0.42)).called(1);
          verify(() => hiveStorageInterface.setPitch(0.42)).called(1);
        },
      );
    });

    group('selectLanguage', () {
      final spanish = Languages.spanish;
      blocTest<SettingsCubit, SettingsState>(
        'stops tts; sets language in data-model; stores flag; sets language in tts; emits state',
        build: () => cubit,
        act: (cubit) => cubit.selectLanguage(spanish),
        expect: () => [SettingsState(language: spanish)],
        verify: (_) {
          verify(textToSpeechInterface.stop).called(2); // 1 triggered in tearDown()/close()
          verify(() => dataModel.setLanguage(spanish)).called(1);
          verify(() => hiveStorageInterface.saveLanguageLocaleToStorage(spanish.locale)).called(1);
          verify(() => textToSpeechInterface.setLanguage(spanish)).called(1);
        },
      );
    });
  });
}
