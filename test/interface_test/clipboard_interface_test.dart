import 'package:talk_to_me/interfaces/clipboard_interface.dart';
import 'package:test/test.dart';

void main() {
  group('ClipboardInterface', () {
    test('method exists & returns type (cannot mock the underlying Clipboard)', () async {
      const clipboardInterface = ClipboardInterface();
      expect(clipboardInterface.getText.runtimeType.toString(), "() => Future<Iterable<String>>");
    });
  });
}
