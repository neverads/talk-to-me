import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:talk_to_me/cubit/text_to_speech_cubit.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/clipboard_interface.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/interfaces/web_utils.dart';
import 'package:talk_to_me/language.dart';
import 'package:talk_to_me/ui/settings_screen.dart';

final transparentGrey = Colors.grey.withOpacity(0.15);

class HomeScreen extends StatelessWidget {
  final Language initialLanguage;
  final String appTitle;

  const HomeScreen({required this.initialLanguage, required this.appTitle});

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (BuildContext context) => TextToSpeechCubit(
          RepositoryProvider.of<ClipboardInterface>(context),
          RepositoryProvider.of<HiveStorageInterface>(context),
          RepositoryProvider.of<TextToSpeechInterface>(context),
          RepositoryProvider.of<WebUtils>(context),
          DataModel(language: initialLanguage),
        ),
        child: HomeView(appTitle: appTitle),
      );
}

class HomeView extends StatefulWidget {
  final String appTitle;

  const HomeView({required this.appTitle});

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  static const popupMenuItemValueReset = 9990;
  static const popupMenuItemValueSettings = 9991;

  static const double iconSizeSmall = 32.0;
  static const double iconSizeMedium = 48.0;
  static const double iconSizeLarge = 60.0;

  final longDuration = const Duration(milliseconds: 500);
  final shortDuration = const Duration(milliseconds: 300);
  late AnimationController playPauseAnimationController = AnimationController(
    duration: longDuration,
    reverseDuration: shortDuration,
    vsync: this,
  );
  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();

  TextToSpeechCubit get cubit => context.read<TextToSpeechCubit>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    cubit.playPauseAnimationController = playPauseAnimationController;
    cubit.setPitchFromStorage();
    cubit.setSpeechRateFromStorage();
    cubit.setScrollNextToActiveSectionCallback(scrollNextToIndexOfActiveSectionIfNeeded);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    cubit.setIsAppInForeground(state == AppLifecycleState.resumed);
  }

  @override
  dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    playPauseAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      try {
        itemScrollController.jumpTo(index: cubit.indexOfActiveSection);
      } catch (_) {}
    });
    return Scaffold(
      appBar: appBar,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: BlocBuilder<TextToSpeechCubit, TextToSpeechState>(
                builder: (context, state) {
                  if (state is TextToSpeechStateLoaded) {
                    // ^^ also handles TextToSpeechStateInitial, which inherits from TextToSpeechStateLoaded
                    return DisplayTextViewLoaded(
                      textToDisplay: state.text,
                      indexOfActiveSection: state.indexOfActiveSection,
                      itemScrollController: itemScrollController,
                      itemPositionsListener: itemPositionsListener,
                    );
                  } else {
                    // DisplayTextStateLoading
                    return const DisplayTextViewLoading();
                  }
                },
              ),
            ),
            BlocBuilder<TextToSpeechCubit, TextToSpeechState>(
              builder: (context, state) {
                if (state is TextToSpeechStateLoaded) {
                  // ^^ also handles TextToSpeechStateInitial, which inherits from TextToSpeechStateLoaded
                  return ProgressWidget(
                    percentComplete: state.percentComplete,
                    percentInActiveSection: cubit.percentInActiveSection,
                  );
                } else {
                  // DisplayTextStateLoading
                  return ProgressWidget(
                    percentComplete: null,
                    percentInActiveSection: null,
                  );
                }
              },
            ),
            audioControlsWidget,
          ],
        ),
      ),
    );
  }

  AppBar get appBar {
    final EdgeInsets _padding = const EdgeInsets.symmetric(horizontal: 24);
    return AppBar(
      title: Text(widget.appTitle),
      actions: [
        IconButton(
          icon: const Icon(Icons.paste),
          onPressed: cubit.pasteTextFromClipboard,
        ),
        PopupMenuButton(
          itemBuilder: (BuildContext context) => [
            PopupMenuItem(
              value: popupMenuItemValueReset,
              padding: _padding,
              child: Icon(
                Icons.delete_sweep,
                color: Theme.of(context).primaryColor,
              ),
            ),
            PopupMenuItem(
              value: popupMenuItemValueSettings,
              padding: _padding,
              child: Icon(
                Icons.settings,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ],
          onSelected: (value) async {
            if (value == popupMenuItemValueReset) {
              cubit.reset();
            } else if (value == popupMenuItemValueSettings) {
              await cubit.navigateToSettings();
              final bool wasExplanationText = cubit.isExplanationText;
              // this navigation is untested!
              await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => SettingsScreen(
                    initialLanguage: cubit.getLanguage(),
                  ),
                ),
              );
              cubit.navigateFromSettings(wasExplanationText); // this line is untested!
            }
          },
        ),
      ],
    );
  }

  Widget get audioControlsWidget => Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(),
              const SizedBox(),
              skipToStartButton,
              previousButton,
              playPauseButton,
              nextButton,
              skipToEndButton,
              const SizedBox(),
              const SizedBox(),
            ],
          ),
        ),
      );

  Widget get skipToStartButton => GestureDetector(
        child: IconButton(
          iconSize: iconSizeSmall,
          icon: const Icon(
            Icons.fast_rewind,
          ),
          onPressed: () {
            ScaffoldMessenger.of(context).clearSnackBars();
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(cubit.getLanguage().jumpToStartText),
              ),
            );
          },
        ),
        onLongPress: () async {
          await cubit.skipToFirstSection();
          scrollToIndexOfActiveSectionIfItIsNotVisible(isGoingToPreviousSection: false);
        },
      );

  Widget get previousButton => IconButton(
        iconSize: iconSizeMedium,
        icon: const Icon(
          Icons.skip_previous,
        ),
        onPressed: () async {
          await cubit.goToPreviousSection();
          scrollToIndexOfActiveSectionIfItIsNotVisible(isGoingToPreviousSection: true);
        },
      );

  Widget get playPauseButton => IconButton(
        iconSize: iconSizeLarge,
        icon: AnimatedIcon(
          icon: AnimatedIcons.play_pause,
          progress: playPauseAnimationController,
        ),
        onPressed: cubit.playPause,
      );

  Widget get nextButton => IconButton(
      iconSize: iconSizeMedium,
      icon: const Icon(
        Icons.skip_next,
      ),
      onPressed: () async {
        await cubit.goToNextSection();
        scrollNextToIndexOfActiveSectionIfNeeded();
      });

  Widget get skipToEndButton => GestureDetector(
        child: IconButton(
          iconSize: iconSizeSmall,
          icon: const Icon(
            Icons.fast_forward,
          ),
          onPressed: () {
            ScaffoldMessenger.of(context).clearSnackBars();
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(cubit.getLanguage().jumpToEndText),
              ),
            );
          },
        ),
        onLongPress: () async {
          await cubit.skipToLastSection();
          scrollToIndexOfActiveSectionIfItIsNotVisible(isGoingToPreviousSection: false);
        },
      );

  List<int> get completelyVisibleSectionIndexes {
    final List<int> indexes = itemPositionsListener.itemPositions.value
      .where((e) => e.itemLeadingEdge >= 0 && e.itemTrailingEdge <= 1)
      .map((e) => e.index)
      .toList();
    indexes.sort();
    return indexes;
  }

  void scrollToIndex(Duration duration) {
    if (cubit.isAppInForeground) {
      itemScrollController.scrollTo(
        index: cubit.indexOfActiveSection,
        duration: duration,
        curve: Curves.easeInOut,
      );
    } else {
      itemScrollController.jumpTo(index: cubit.indexOfActiveSection);
    }
  }

  void scrollNextToIndexOfActiveSectionIfNeeded() {
    final int indexOfActiveSection = cubit.indexOfActiveSection;
    final bool shouldScroll = !completelyVisibleSectionIndexes.contains(indexOfActiveSection) ||
        completelyVisibleSectionIndexes.last != cubit.maxPossibleIndex;
    if (shouldScroll) {
      scrollToIndex(
        cubit.getScrollDurationForSectionAtIndex(indexOfActiveSection - 1) * 1.2,
      );
    }
  }

  void scrollToIndexOfActiveSectionIfItIsNotVisible({required bool isGoingToPreviousSection}) {
    final int indexOfActiveSection = cubit.indexOfActiveSection;
    final bool shouldScroll = !completelyVisibleSectionIndexes.contains(indexOfActiveSection);
    if (shouldScroll) {
      scrollToIndex(
        isGoingToPreviousSection
            ? cubit.getScrollDurationForSectionAtIndex(indexOfActiveSection)
            : Duration(milliseconds: 500),
      );
    }
  }
}

class DisplayTextViewLoaded extends StatelessWidget {
  final Iterable<String> textToDisplay;
  final int indexOfActiveSection;
  final ItemScrollController itemScrollController;
  final ItemPositionsListener itemPositionsListener;

  const DisplayTextViewLoaded({
    required this.textToDisplay,
    required this.indexOfActiveSection,
    required this.itemScrollController,
    required this.itemPositionsListener,
  });

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(color: transparentGrey),
        child: ScrollablePositionedList.builder(
          itemScrollController: itemScrollController,
          itemPositionsListener: itemPositionsListener,
          itemCount: textToDisplay.length,
          itemBuilder: (context, index) {
            final isActiveSection = index == indexOfActiveSection;
            final highlightMarkOpacity = isActiveSection ? 0.5 : 0.0;
            final textOpacity = isActiveSection ? 1.0 : 0.8;
            return Padding(
              padding: EdgeInsets.fromLTRB(12, 16, 12, index == textToDisplay.length - 1 ? 64 : 0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(
                      color: Theme.of(context).accentColor.withOpacity(highlightMarkOpacity),
                      width: 2,
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                  child: Opacity(
                    opacity: textOpacity,
                    child: Text(
                      textToDisplay.elementAt(index),
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(height: 2),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );
}

class ProgressWidget extends StatelessWidget {
  final int? percentComplete;
  final int? percentInActiveSection;

  const ProgressWidget({this.percentComplete, this.percentInActiveSection});

  @override
  Widget build(BuildContext context) {
    final bool shouldDefaultTo0 = percentComplete == null || percentInActiveSection == null;
    final int combinedPercent = shouldDefaultTo0 ? 0 : percentComplete! + percentInActiveSection!;
    final Color baseColor = Theme.of(context).primaryColor;

    return Stack(
      children: [
        LinearProgressIndicator(
          value: combinedPercent / 100,
          color: baseColor,
          backgroundColor: baseColor.withOpacity(0.3),
        ),
        LinearProgressIndicator(
          value: (percentComplete ?? 0) / 100,
          color: Color.alphaBlend(Colors.white38, baseColor),
          backgroundColor: Colors.transparent,
        ),
      ],
    );
  }
}

class DisplayTextViewLoading extends StatelessWidget {
  const DisplayTextViewLoading();

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(color: transparentGrey),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                SizedBox(height: 24),
                SizedBox(
                  width: 48,
                  height: 48,
                  child: CircularProgressIndicator(strokeWidth: 2),
                ),
                SizedBox(height: 24),
                Text('Getting text…'),
              ],
            ),
          ],
        ),
      );
}
