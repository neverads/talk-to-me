import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:talk_to_me/data_model.dart';
import 'package:talk_to_me/interfaces/hive_interface.dart';
import 'package:talk_to_me/interfaces/text_to_speech_interface.dart';
import 'package:talk_to_me/language.dart';

part 'settings_state.dart';

class SettingsCubit extends Cubit<SettingsState> {
  final TextToSpeechInterface _textToSpeechInterface;
  final HiveStorageInterface _hiveInterface;
  final DataModel _dataModel;

  SettingsCubit(
    this._textToSpeechInterface,
    this._hiveInterface,
    this._dataModel,
  ) : super(SettingsState(language: _dataModel.language)) {
    _textToSpeechInterface.clearCompletionCallback();
  }

  double get speechRate => _hiveInterface.speechRate;

  double get pitch => _hiveInterface.pitch;

  @override
  Future<void> close() async {
    _textToSpeechInterface.stop();
    _textToSpeechInterface.restoreCompletionCallback();
    return super.close();
  }

  Future<void> stopSpeaking() async => await _textToSpeechInterface.stop();

  Future<void> speakSampleText() async {
    final language = _dataModel.getLanguage();
    await _textToSpeechInterface.speak(language.sampleText);
  }

  Future<void> setSpeechRate(double newSpeechRate) async {
    await _textToSpeechInterface.setSpeechRate(newSpeechRate);
    _hiveInterface.setSpeechRate(newSpeechRate);
  }

  Future<void> setPitch(double newPitch) async {
    await _textToSpeechInterface.setPitch(newPitch);
    _hiveInterface.setPitch(newPitch);
  }

  void selectLanguage(Language language) {
    _textToSpeechInterface.stop();
    _dataModel.setLanguage(language);
    _hiveInterface.saveLanguageLocaleToStorage(language.locale);
    _textToSpeechInterface.setLanguage(language);
    emit(SettingsState(language: language));
  }
}
