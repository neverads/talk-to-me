import 'package:flutter/foundation.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:talk_to_me/language.dart';

class TextToSpeechInterface {
  TextToSpeechInterface({FlutterTts? textToSpeech}) : _textToSpeech = textToSpeech ?? FlutterTts();

  final FlutterTts _textToSpeech;

  VoidCallback _completionCallback = () {};

  Future<void> speak(String text) async => await _textToSpeech.speak(text);

  Future<void> stop() async => await _textToSpeech.stop();

  void setCompletionCallback(VoidCallback callback) {
    _completionCallback = callback;
    _textToSpeech.setCompletionHandler(callback);
  }

  void clearCompletionCallback() => _textToSpeech.setCompletionHandler(() {});

  void restoreCompletionCallback() => _textToSpeech.setCompletionHandler(_completionCallback);

  Future<void> setSpeechRate(double newRate) async {
    await _textToSpeech.setSpeechRate(newRate);
  }

  Future<void> setPitch(double newRate) async {
    await _textToSpeech.setPitch(newRate);
  }

  void setLanguage(Language language) {
    if (Languages.allSupported.contains(language)) {
      _textToSpeech.setLanguage(language.locale);
    } else {
      _textToSpeech.setLanguage(Languages.english.locale);
    }
  }
}
